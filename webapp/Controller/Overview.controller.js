sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "../Utils/Formatter",
    "../Utils/Config",
    "../Utils/DataAccess",
    "sap/m/Label",
    "sap/m/ColumnListItem"
], function (JSONModel, Controller, MessageBox, Formatter, Config, DataAccess, Label, ColumnListItem) {
    "use strict";

    return Controller.extend("zhcm_Hap_650.Controller.Overview", {

        formatter: Formatter,
        filterMemo: {},
        _dialogs: [],

        onInit: function () {
            // #DEBUG START
            // if (window.location.hostname.toLowerCase() === "sms00990" ||
            //     window.location.hostname === "127.0.0.1" ||
            //     window.location.hostname.toLowerCase() === "localhost") window.goControllerOv = this;
            // #DEBUG END

            console.log('BEGIN Overview.onInit');

            if(this._switchViewIfMobile()) {
                return;
            }

            this.getOwnerComponent().getRouter().getRoute("overview")
                .attachMatched(this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("mOverview")
                .attachMatched(this._onRouteMatched, this);

            var viewData = {
                displayСompleted: true,
                SubordinatesTabVisible: false,
                MyMarksTabVisible: false,
                ReportTabVisible: false,
                activeTab: "MyTasks"
            };

            var oViewModel = new JSONModel(viewData);
            this.getView().setModel(oViewModel, "viewData");

            var oYears = new JSONModel(this._getYears(2017, new Date().getFullYear()));
            this.getView().setModel(oYears, "Years");

            // ModelDialog
            var oModelDialog = new JSONModel();
            this.getView().setModel(oModelDialog, "ModelDialog");

            console.log('END Overview.onInit');
        },

        _onRouteMatched: function () {
            console.log('BEGIN Overview._onRouteMatched');

            var that = this;
            if (this.filterMemo.hasOwnProperty("PernrMemId")) {
                this.getView().setBusy(true);
                DataAccess.getMyTasks(this.filterMemo.PernrMemId).then(
                    function (oMyTasksResult) {
                        that._bindJSONModel("MyTasks", oMyTasksResult);
                        that.getView().setBusy(false);
                    },
                    function (oError) {
                        that.getView().setBusy(false);
                    }
                );
            } else {
                this._initialDataRead();
            }
            console.log('END Overview._onRouteMatched');
        },

        _initialDataRead: function () {
            console.log('BEGIN Overview._initialDataRead');

            var that = this,
                setBusy = function (bVal) {
                    this.getView().setBusy(bVal);
                }.bind(this);

            setBusy(true);

            this._updateChipData();

            DataAccess.getUserMemId().then(
                function (aPernr) {
                    var sPernrMemId = aPernr[0].PERNR_MEMID,
                        sPernr = aPernr[0].DESCR.match(/[0-9]{8}/g)[0];

                    that._setAppData("/CurrentPernrMemId", sPernrMemId);
                    that._setAppData("/CurrentPernr", sPernr);
                    that._bindJSONModel("PernrsMemIdSet", aPernr);
                    // lets set initial filter values - representing keys that was used for initial data read of those tabs
                    that._setInitialFilterMemo(sPernrMemId);

                    return that._getOverviewTabsData(sPernrMemId);
                }
            ).then(
                function (oTasksResult, oMarksResult, oSubordResults) { // success
                    setBusy(false);
                },
                function () { // error
                    setBusy(false); // no error handling
                }
            );

            console.log('END Overview._initialDataRead');
        },

        _updateChipData: function () {
            console.log('BEGIN Overview._updateChipData');

            var oViewModel = this.getView().getModel("viewData");
            DataAccess.getChipData().then(
                function (aChipData) {
                    oViewModel.setProperty("/TasksAmount", aChipData[0].TASKS_AMOUNT);

                    // в PERNRS_AM_LIST приходит строка вида [{"11111111":19 },{"22222222":1 }],
                    var PERNRS_AM_LIST = aChipData[0].PERNRS_AM_LIST, aPERNRS_AM_LIST;
                    if (PERNRS_AM_LIST != "") {
                        aPERNRS_AM_LIST = JSON.parse(PERNRS_AM_LIST);
                    }

                    oViewModel.setProperty("/PernrsAmList", aPERNRS_AM_LIST);
                }
            );
            console.log('END Overview._updateChipData');
        },

        _setInitialFilterMemo: function (sPernrMemId) {
            console.log('BEGIN Overview._setInitialFilterMemo');

            this.filterMemo = {
                PernrMemId: sPernrMemId,
                Year: "",
                ProcStatus: "",
                SelSubordPernr: ""
            };
            console.log('END Overview._setInitialFilterMemo');
        },

        _switchViewIfMobile: function () {
            console.log('BEGIN Overview._switchViewIfMobile');

            if (this.getOwnerComponent().getModel("device").getProperty("/isPhone")
                && this._isCurrentViewForDesktop()) {
                this.getOwnerComponent().getRouter().navTo("mOverview", null, null, true);
                console.log('END Overview._switchViewIfMobile');
                return true;
            }
        },

        _isCurrentViewForDesktop: function(){
            console.log('BEGIN Overview._isCurrentViewForDesktop');
            var bReturn = this.getView().getViewName().split(".").slice(-1)[0] === "Overview"
                ? true
                : false;
            console.log('END Overview._isCurrentViewForDesktop');

            return bReturn;
        },

        _getOverviewTabsData: function (sPernrMemId) {
            console.log('BEGIN Overview._getOverviewTabsData');

            var bTabVisible,
                that = this;

            var getMyTasks = DataAccess.getMyTasks(sPernrMemId).done(
                function (oMyTasksResult) {
                    console.log('BEGIN Overview._getOverviewTabsData.getMyTasks.done');
                    that._bindJSONModel("MyTasks", oMyTasksResult);
                    console.log('END Overview._getOverviewTabsData.getMyTasks.done');
                }
            );
            var getMyMarks = DataAccess.getMyMarks(sPernrMemId).done(
                function (oMyMarksResult) {
                    console.log('BEGIN Overview._getOverviewTabsData.getMyMarks.done');
                    that._bindJSONModel("MyMarks", oMyMarksResult);
                    bTabVisible = oMyMarksResult.length > 0 ? true : false;
                    that.getView().getModel("viewData").setProperty("/MyMarksTabVisible", bTabVisible);
                    console.log('END Overview._getOverviewTabsData.getMyMarks.done');
                }
            );
            var getSubordRatings = DataAccess.getMySubordinates(sPernrMemId).done(
                function (aSubordResults) {
                    console.log('BEGIN Overview._getOverviewTabsData.getSubordRatings.done');
                    bTabVisible = false;
                    if (aSubordResults.length > 0) {
                        that._bindJSONModel("MySubordinates", aSubordResults);
                        that._populateSubordFilter(aSubordResults);
                        that.filterMemo.subordsFullResult = aSubordResults;
                        bTabVisible = true;
                    }
                    that.getView().getModel("viewData").setProperty("/SubordinatesTabVisible", bTabVisible);
                    console.log('END Overview._getOverviewTabsData.getSubordRatings.done');
                }
            );

            DataAccess.getMyReports(sPernrMemId).done(
                function (aReportResults) {
                    console.log('BEGIN Overview._getOverviewTabsData.getMyReports.done');
                    bTabVisible = aReportResults.length > 0 ? true : false;
                    that.getView().getModel("viewData").setProperty("/ReportTabVisible", bTabVisible);
                    $.each(aReportResults, function(sIndex, oRep ) {
                        oRep.PERNR = "";
                        oRep.ID_EVAL_PROC = "";
                    });
                    that._bindJSONModel("MyReports", aReportResults);
                    console.log('END Overview._getOverviewTabsData.getMyReports.done');
                }
            );
            console.log('END Overview._getOverviewTabsData');
            return $.when(getMyTasks, getMyMarks, getSubordRatings);
        },


        _getYears: function (iStartYear, iCurrYear) {
            console.log('BEGIN Overview._getYears');

            var aYears = [];
            do {
                aYears.push(iStartYear);
                iStartYear += 1;
            }
            while (iStartYear <= iCurrYear + 1);

            console.log('END Overview._getYears');
            return aYears;
        },

        _bindJSONModel: function (sModelName, oData) {
            console.log('BEGIN Overview._bindJSONModel');

            var oModel = this.getView().getModel(sModelName);
            if (!oModel) {
                oModel = new JSONModel(oData);
                this.getView().setModel(oModel, sModelName);
            } else {
                oModel.setData(oData);
            }
            console.log('END Overview._bindJSONModel');
        },

        _setAppData: function (sPropName, Value) {
            console.log('BEGIN Overview._setAppData');

            this.getOwnerComponent().getModel("AppData").setProperty(sPropName, Value);
            console.log('END Overview._setAppData');
        },

        _getAppData: function (sProperty) {
            console.log('BEGIN Overview._getAppData');

            return this.getOwnerComponent().getModel("AppData").getProperty(sProperty);
            console.log('END Overview._getAppData');
        },

        _populateSubordFilter: function (oSubordRes) {
            console.log('BEGIN Overview._populateSubordFilter');

            var aUnqiquePernrs = [];
            var aUniqueSubords = [];
            $.each(oSubordRes, function (iInd, oPanel) {
                $.each(oPanel.ToApSubrTable.results, function (iInd, oSubordinate) {
                    if (aUnqiquePernrs.indexOf(oSubordinate.PERNR) == -1) {
                        aUniqueSubords.push(oSubordinate);
                        aUnqiquePernrs.push(oSubordinate.PERNR);
                    }
                });
            });

            this._bindJSONModel("Subordinates", aUniqueSubords);
            console.log('END Overview._populateSubordFilter');
        },

        onApplyFilters: function () {
            console.log('BEGIN Overview.onApplyFilters');

            // if PernrMemId or year changed -> reread data and save filter values
            var sSelectedTabKey = this.getView().byId("IconTabBar").getSelectedKey(),
                mFilterControls = this._getFilterControls(),
                sPernrMemId = mFilterControls.selPernrMemId.getSelectedKey(),
                sPernr,
                sYear = mFilterControls.selYear.getSelectedKey() || mFilterControls.selYear.getValue(),
                sProc = mFilterControls.selProcStatus.getSelectedKey(),
                oView = this.getView(),
                sSubord = false,
                bReReadOccured = false;

            if (mFilterControls.selSubord) {
                sSubord = mFilterControls.selSubord.getSelectedKey();
            }

            this.filterMemo.ProcStatus = sProc;
            this.filterMemo.Year = sYear;

            // Overview table holds data only for single combination of PERNR_MEMID and YEAR, 
            // so we re-read
            if (this.filterMemo.PernrMemId !== sPernrMemId) {
                this.filterMemo.PernrMemId = sPernrMemId;
                sPernr = this.getView().getModel("PernrsMemIdSet").getData().filter(
                        function (oPernr) { 
                            return oPernr.PERNR_MEMID === sPernrMemId; 
                        }
                    )[0].DESCR.match(/[0-9]{8}/g)[0];

                this._setAppData("/CurrentPernr", sPernr);
                this._setAppData("/CurrentPernrMemId", sPernrMemId);
                this.getView().setBusy(true);
                this._getOverviewTabsData(sPernrMemId)
                    .done(oView.setBusy.bind(oView, false));

                bReReadOccured = true;
            }

            var aFilters = [];

            if (sProc) {
                aFilters.push(new sap.ui.model.Filter(
                        "STATUS_EVAL_PROC", 
                        sap.ui.model.FilterOperator.EQ, 
                        sProc
                    )
                );
            }
            if (sYear) {
                aFilters.push(new sap.ui.model.Filter(
                        "YEAR", 
                        sap.ui.model.FilterOperator.EQ, 
                        sYear
                    )
                );
            }

            Config.overviewContainerTabs.forEach(function (containerName) {
                var oViewContainer = this.getView().byId(containerName),
                    b = oViewContainer.getBinding("content");
                if (b) {
                    b.filter(aFilters);
                }
            }.bind(this));

            if (sSubord && !bReReadOccured) {
                // if it occured - filtering already took place in oData callback(readTabDataWithNewKeys)
                var aFiltered = this._applyManualSubordinateFilter(sSubord, this.filterMemo.subordsFullResult);
                this._bindJSONModel(sSelectedTabKey, aFiltered);
            }

            this._updateChipData();
            console.log('END Overview.onApplyFilters');
        },


        _getFilterControls: function () {
            console.log('BEGIN Overview._getFilterControls');

            var oSelPernrMemId = this.getView().byId("selPernrMemId") || sap.ui.getCore().byId("selPernrMemId");
            var oSelYear = this.getView().byId("selYear") || sap.ui.getCore().byId("selYear");
            var oSelProcStatus = this.getView().byId("selProcStatus") || sap.ui.getCore().byId("selProcStatus");
            var oSelSubord = this.getView().byId("subordFilter") || sap.ui.getCore().byId("subordFilter");

            if (this._oFilterDialog) { // mobileUI
                oSelPernrMemId = sap.ui.core.Fragment.byId("idFiltersDialog", "selPernrMemId");
                oSelYear = sap.ui.core.Fragment.byId("idFiltersDialog", "selYear");
                oSelProcStatus = sap.ui.core.Fragment.byId("idFiltersDialog", "selProcStatus");
                oSelSubord = sap.ui.core.Fragment.byId("idFiltersDialog", "subordFilter");
            }
            console.log('END Overview._getFilterControls');
            return {
                selPernrMemId: oSelPernrMemId,
                selYear: oSelYear,
                selProcStatus: oSelProcStatus,
                selSubord: oSelSubord
            };
        },

        restoreFilterValueByTabs: function (sSelectedTab) {
            console.log('BEGIN Overview.restoreFilterValueByTabs');

            var mFilterControls = this._getFilterControls(),
                oSelectedTab = this.filterMemo[sSelectedTab];

            if (mFilterControls.selPernrMemId) {
                mFilterControls.selPernrMemId.setSelectedKey(oSelectedTab.Pernr);
            }
            if (mFilterControls.selYear) {
                mFilterControls.selYear.setSelectedKey(oSelectedTab.Year);
            }
            if (mFilterControls.selProcStatus) {
                mFilterControls.selProcStatus.setSelectedKey(oSelectedTab.ProcStatus);
            }
            if (mFilterControls.selSubord) {
                mFilterControls.selSubord.setSelectedKey(oSelectedTab.SelSubordPernr);
            }
            console.log('END Overview.restoreFilterValueByTabs');
        },

        _applyManualSubordinateFilter: function (sSubordPernr, oFullResult) {
            console.log('BEGIN Overview._applyManualSubordinateFilter');

            var aFilteredMySubordinates = [];
            $.each(oFullResult, function (iInd, oProcPanel) {
                var aFiltered = oProcPanel.ToApSubrTable.results.filter(
                    function (mEmpl) {
                        if (mEmpl.PERNR === sSubordPernr) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                );
                if (aFiltered.length > 0) {
                    var oFilteredPanel = $.extend({}, oProcPanel);
                    delete oFilteredPanel.ToApSubrTable;
                    oFilteredPanel.ToApSubrTable = {};
                    oFilteredPanel.ToApSubrTable.results = aFiltered;
                    aFilteredMySubordinates.push(oFilteredPanel);
                }
            });
            console.log('END Overview._applyManualSubordinateFilter');
            return aFilteredMySubordinates;
        },

        onIconTabBarSelect: function (oEvt) {
            console.log('BEGIN Overview.onIconTabBarSelect');

            // construct filter screen
            var sTabId = oEvt.getParameter("key"),
                oFilterContainer = this.byId("FiltersContainer");
            if (oFilterContainer) {
                oFilterContainer.setExpanded(false);
                oFilterContainer.destroyContent();

                if (Config.filterFragments[sTabId]) {
                    var oFilterFragment = sap.ui.xmlfragment(Config.filterFragments[sTabId], this);
                    oFilterContainer.addContent(oFilterFragment);
                }
            }

            this.getView().getModel("viewData").setProperty("/activeTab", sTabId);
            console.log('END Overview.onIconTabBarSelect');
        },

        onAppraisalItemPress: function (oEvt) {
            console.log('BEGIN Overview.onAppraisalItemPress');

            var oSrc = oEvt.getSource(),
                oSelectedTask = oSrc.getModel("MyTasks").getProperty(oSrc.getBindingContextPath()),
                sProcPath = oSrc.getParent().getParent().getBindingContext("MyTasks").getPath(),
                oProcedure = oSrc.getModel("MyTasks").getProperty(sProcPath);

            this._setAppData.call(this.oView.getController(), "/TaskTxt", oSelectedTask.TASK_TXT);
            this._setAppData.call(this.oView.getController(), "/NameEvalProc", oProcedure.NAME_EVAL_PROC);

            this.onNavigate(this._getNavTargetName(oSelectedTask.NEXT_INTERFACE), {
                "AppraisalId": oSelectedTask.APPRAISAL_ID,
                "IndicatorId": oSelectedTask.INDICATOR_ID
            });
            console.log('END Overview.onAppraisalItemPress');
        },

        _getNavTargetName: function(sNextInterface){
            console.log('BEGIN Overview._getNavTargetName');

            var sMobilePrefix =
                this.getOwnerComponent().getModel("device").getProperty("/isPhone")
                    ? "m"
                    : "";
            console.log('END Overview._getNavTargetName');
            return sMobilePrefix + Config.interfaceIds[sNextInterface];
        },

        onNavigate: function (sTarget, oParams) {
            console.log('BEGIN Overview.onNavigate');

            this.getOwnerComponent().getRouter().navTo(sTarget, oParams);
            console.log('END Overview.onNavigate');
        },

        onMarkIconHelpPress: function (oEvt) {
            console.log('BEGIN Overview.onMarkIconHelpPress');

            var oControl = oEvt.getSource(),
                sScaleBindingPath,
                oPopoverData,
                sFragmentName = "zhcm_Hap_650.fragments.MyMarksScale";

            var fMapMyMarksToPopoverM = function (oMyMarksInput) {
                return {
                    RAT_LOW: oMyMarksInput.RAT_LOW,
                    RAT_HIGH: oMyMarksInput.RAT_HIGH,
                    RAT_DIST: oMyMarksInput.RAT_DIST,
                    RAT_TEXT: oMyMarksInput.RAT_TEXT,
                    Ranges: oMyMarksInput.ToMarkScaleRange.results || []
                };
            };

            var fCreateAndOpenPopover = function (sFragmentName, oEvt, oController) {
                oController._oMarksScalePopover = sap.ui.xmlfragment(sFragmentName, this);
                oController.getView().addDependent(oController._oMarksScalePopover);

                oController._oMarksScalePopover.attachAfterClose(function () {
                    oController._oMarksScalePopover.destroy();
                    oController._oMarksScalePopover = null;
                }, oController);
                oController._oMarksScalePopover.openBy(oEvt.getSource());
            };

            // get certain Panel's marks model, which contains Scale's data
            do {
                oControl = oControl.getParent();
            } while (oControl.getId().indexOf("panel") === -1);

            var oContext = oControl.getBindingContext("MyMarks");
            var oMarksData = oContext.getModel().getProperty(oContext.getPath());

            switch (oMarksData.SCALE_ART) {
                case "M":
                    sScaleBindingPath = oContext.getPath() + "/ToCommentScaleM/results/0/";
                    sFragmentName += "M";
                    oPopoverData = fMapMyMarksToPopoverM(oContext.getModel().getProperty(sScaleBindingPath));
                    break;
                case "Q":
                    sScaleBindingPath = oContext.getPath() + "/ToCommentScaleQ/results/";
                    sFragmentName += "Q";
                    oPopoverData = oContext.getModel().getProperty(sScaleBindingPath);
                    break;
                default:
                    break;
            }

            sScaleBindingPath = oContext.getPath() + "/DESCRIPTION";
            var sDescr = oContext.getProperty(sScaleBindingPath);
            if (sDescr && sDescr.length > 0) {
                var lastChr = sFragmentName.toString().slice(-1);
                if (lastChr && (lastChr === "M" || lastChr === "Q")) {
                    sFragmentName = sFragmentName.substring(0, sFragmentName.length - 1);
                }
                sFragmentName += "C";
                oPopoverData = [
                    { DESCRIPTION: sDescr }
                ];
            }

            this._bindJSONModel("PopoverScale", oPopoverData);
            fCreateAndOpenPopover(sFragmentName, oEvt, this);
            console.log('END Overview.onMarkIconHelpPress');
        },

        onSubordHelp: function (oEvt) {
            console.log('BEGIN Overview.onSubordHelp');

            this._oSubordDialog = sap.ui.xmlfragment("zhcm_Hap_650.fragments.SubordValueHelpDialog", this);
            this._oSubordDialog.addStyleClass('ZHCM_PM_0650');
            this.getView().addDependent(this._oSubordDialog);
            var oInput = oEvt.getSource();
            this._oSubordDialog.callerInput = oInput;
            var sRowsAggregationName, fnItemsFactory;

            var oValueHelpCust = Config.valueHelpCust.filter(function (custItem) {
                return oInput.getId().indexOf(custItem.inputId) > -1;
            })[0];
            var aCols = oValueHelpCust.cols;
            var oColModel = new JSONModel({ cols: aCols });

            var oTable = this._oSubordDialog.getTable();
            oTable.setModel(oColModel, "columns");
            oTable.setModel(this.getView().getModel(oValueHelpCust.model));

            switch (oTable.getMetadata().getName()) {
                case "sap.ui.table.Table":
                    sRowsAggregationName = "rows";
                    fnItemsFactory = null;
                    break;
                case "sap.m.Table":
                    sRowsAggregationName = "items";
                    fnItemsFactory = function () {
                        return new ColumnListItem({
                            cells: aCols.map(function (column) {
                                return new Label({ text: "{" + column.template + "}" });
                            })
                        });
                    };
                    break;
                default:
                    break;
            }

            oTable.bindAggregation(sRowsAggregationName, "/", fnItemsFactory);

            this._oSubordDialog.setKey(oValueHelpCust.key);
            this._oSubordDialog.setDescriptionKey(oValueHelpCust.descrKey);

            if (oInput.getSelectedKey()) {
                this._oSubordDialog.setTokens([
                    new sap.m.Token({
                        key: oInput.getSelectedKey(),
                        text: oInput.getValue()
                    })
                ]);
            }

            this._oSubordDialog.update();
            this._oSubordDialog.open();
            console.log('END Overview.onSubordHelp');
        },

        onSubordHelpOk: function (oEvent) {
            console.log('BEGIN Overview.onSubordHelpOk');

            var aTokens = oEvent.getParameter("tokens");
            this._oSubordDialog.callerInput.setSelectedKey(aTokens[0].getKey());
            this._oSubordDialog.close();
            console.log('END Overview.onSubordHelpOk');
        },

        onSubordHelpCancel: function () {
            console.log('BEGIN Overview.onSubordHelpCancel');

            this._oSubordDialog.close();
            console.log('END Overview.onSubordHelpCancel');
        },

        onSubordHelpAfterClose: function () {
            console.log('BEGIN Overview.onSubordHelpAfterClose');

            this._oSubordDialog.destroy();
            console.log('END Overview.onSubordHelpAfterClose');
        },

        onReportHandlePress: function (oEvent) {
            console.log('BEGIN Overview.onReportHandlePress');

            var oSrc = oEvent.getSource();
            var sPath = oSrc.getParent().getBindingContext("MyReports").getPath();
            var oSelectedLinkData = oSrc.getModel("MyReports").getProperty(sPath);
            this._dialogCreate("FormReportDialog", oSelectedLinkData);
            console.log('END Overview.onReportHandlePress');
        },


        onIndReportFromMarks: function (oEvt) {
            console.log('BEGIN Overview.onIndReportFromMarks');

            var oCont = oEvt.getSource().getParent().getBindingContext("MyMarks"),
                oPanelData = oCont.getModel().getProperty(oCont.getPath()),
                sAppraisal = oPanelData.APPRAISAL_ID,
                sPernr = oPanelData.PERNR || "00000000",
                sIdEvalProc = oPanelData.ID_EVAL_PROC;

            this._getIndividualReport(sAppraisal, sPernr, sIdEvalProc);
            console.log('END Overview.onIndReportFromMarks')
        },

        onIndReportFromSubords: function (oEvt) {
            console.log('BEGIN Overview.onIndReportFromSubords');

            var oCont = oEvt.getSource().getParent().getBindingContext("MySubordinates"),
                oRowData = oCont.getModel().getProperty(oCont.getPath()),
                sAppraisal = oRowData.APPRAISAL_ID,
                sPernr = oRowData.PERNR || "00000000",
                sIdEvalProc = oRowData.ID_EVAL_PROC;

            this._getIndividualReport(sAppraisal, sPernr, sIdEvalProc);
            console.log('END Overview.onIndReportFromSubords');
        },

        _getIndividualReport: function (sAppraisal, sPernr, sIdEvalProc) {
            console.log('BEGIN Overview._getIndividualReport');

            DataAccess.getReportAnonymityCheck(sPernr, sIdEvalProc, sAppraisal).then(
                function (oObj) {
                    console.log('BEGIN Overview.getReportAnonymityCheck success');
                    if (oObj && oObj.FLAG) {
                        DataAccess.getIndividualReport(oObj.PERNR, oObj.ID_EVAL_PROC);
                    } else {
                        MessageBox.show("Недостаточно данных для формирования отчёта. По данной оценке менее 3-х оцененных анкет.", {
                            icon: MessageBox.Icon.INFORMATION,
                            title: "Информация",
                            actions: [MessageBox.Action.OK]
                        });
                    }
                    console.log('END Overview.getReportAnonymityCheck success');
                },
                function (oError) {
                    console.log('Overview.onIndReportFromMarks("getReportAnonymityCheck") > Error: ' + oError);
                }
            );
            console.log('END Overview._getIndividualReport');
        },

        onPrintReportConc: function (oEvt) {
            console.log('BEGIN Overview.onPrintReportConc');

            var oCont = oEvt.getSource().getParent().getBindingContext("MySubordinates"),
                oPanelData = oCont.getModel().getProperty(oCont.getPath()),
                oProcedurePanel = oEvt.getSource().getParent().getParent(),
                aSelectedItems = oProcedurePanel.getContent()[0].getSelectedItems(); // get selected subordinates

            var aSelectedPernrs = aSelectedItems
                .map(function (oItem) {
                    var oRowContext = oItem.getBindingContext("MySubordinates"),
                        oRowData = oRowContext.getModel().getProperty(oRowContext.getPath());
                    return oRowData.PERNR;
                });

            var sCurrentPernrMemId = this._getAppData("/CurrentPernrMemId");

            var sCurrentPernr = this.getView().getModel("PernrsMemIdSet").getData().filter(
                    function (m) {
                        return m.PERNR_MEMID === sCurrentPernrMemId;
                    }
                )[0].DESCR.match(/[0-9]{8}/)[0];

            DataAccess.getConcReport(aSelectedPernrs, oPanelData.ID_EVAL_PROC, sCurrentPernr);
            console.log('END Overview.onPrintReportConc');
        },

        _dialogCreate: function (sDialogName, oData) {
            console.log('BEGIN Overview._dialogCreate');

            var dialog = this._dialogs[sDialogName],
                oView,
                oModel = this.oView.getModel("ModelDialog");
            oModel.setData(oData);
            if (!dialog) {
                oView = sap.ui.xmlview({
                    viewName: "zhcm_Hap_650.view." + sDialogName
                });
                oView.setModel(oModel, "ModelDialog");
                dialog = oView.getContent()[0];
                this.getView().addDependent(dialog);
                this.getView().setModel(oModel, "ModelDialog");
                this._dialogs[sDialogName] = dialog;
            }
            dialog.open();
            console.log('END Overview._dialogCreate');
        },

        onTBIconFilterPress: function (oEvt) {
            console.log('BEGIN Overview.onTBIconFilterPress');

            if (!this._oFilterDialog) {
                this._oFilterDialog = sap.ui.xmlfragment("idFiltersDialog", "zhcm_Hap_650.fragments.mobile.OverviewFiltersDialog", this);
                this.getView().addDependent(this._oFilterDialog);
            }
            this._oFilterDialog.open();
            console.log('END Overview.onTBIconFilterPress');
        },

        onFiltersClose: function () {
            console.log('BEGIN Overview.onFiltersClose');
            this._oFilterDialog.close();
            console.log('END Overview.onFiltersClose');
        },

        onFiltersDialogApply: function (oEvt) {
            console.log('BEGIN Overview.onFiltersDialogApply');
            this.onApplyFilters(oEvt);
            this._oFilterDialog.close();
            console.log('END Overview.onFiltersDialogApply');
        },


    });
});