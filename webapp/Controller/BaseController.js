sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "../Utils/Formatter"
], function (Controller, Formatter) {
    "use strict";

    return Controller.extend("zhcm_Hap_650.Controller.BaseController", {
        formatter: Formatter,

        showUserGuide: function (oEvt) {
            var bIsPhone = this.getOwnerComponent().getModel("device").getProperty("/isPhone"),
                sPopoverFragmentName;

            if (!this._oUserGuideDialogOrPopover) {
                sPopoverFragmentName = bIsPhone 
                    ? "zhcm_Hap_650.fragments.mobile.InformationDialog" 
                    : "zhcm_Hap_650.fragments.UserGuidePopover";

                this._oUserGuideDialogOrPopover = sap.ui.xmlfragment("idUserGuideDialogOrPopover", sPopoverFragmentName, this);
                this._oUserGuideDialogOrPopover.attachAfterClose(function () {
                    this._oUserGuideDialogOrPopover.destroy();
                    this._oUserGuideDialogOrPopover = null;
                }, this);
                this.getView().addDependent(this._oUserGuideDialogOrPopover);

                if (this._oUserGuideDialogOrPopover.openBy) {
                    this._oUserGuideDialogOrPopover.openBy(oEvt.getSource()); 
                 } else { 
                    this._oUserGuideDialogOrPopover.open();
                 }
            } else {
                this._oUserGuideDialogOrPopover.close();
            }
        },

        onUserGuideDialogOrPopoverClose: function () {
            this._oUserGuideDialogOrPopover.close();
        },

        getIsPhone: function () {
            return this.getOwnerComponent().getModel("device").getProperty("/isPhone");
        },
        

    });
});