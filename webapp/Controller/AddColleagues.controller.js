sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "./BaseController",
    "zhcm_Hap_650/Utils/Config",
    "zhcm_Hap_650/Utils/DataAccess",
    "zhcm_Hap_650/Utils/Formatter",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/Filter",
    "sap/m/Token",
    "sap/m/Tokenizer",
    "../Utils/Utilits",
], function (JSONModel, BaseController, Config, DataAccess, Formatter, MessageToast, MessageBox, Filter, Token, Tokenizer, Utilits) {
    "use strict";
    return BaseController.extend("zhcm_Hap_650.Controller.AddColleagues", {
        formatter: Formatter,
        sAppraisalId: "",

        // #DEBUG START
        // _reloadPage: function () {
        //     window.location.reload();
        // },
        // #DEBUG END

        onInit: function () {
            // #DEBUG START
            // if (window.location.hostname.toLowerCase() === "sms00990" ||
            //     window.location.hostname === "127.0.0.1" ||
            //     window.location.hostname.toLowerCase() === "localhost") window.goControllerAc = this;
            // #DEBUG END

            var mViewData = {
                HeaderExpanded: true,
                ColleaguesNum: ""
            };

            var oViewDataModel = new JSONModel(mViewData);
            this.getView().setModel(oViewDataModel, "viewData");

            this.getView().setModel(new JSONModel(), "SearchResults");
            this.getView().setModel(new JSONModel(), "AddedColleagues");
            this.getView().setModel(new JSONModel(), "DeletedColleagues");
            this.getView().setModel(new JSONModel(), "OrgehValueHelpSet");
            this.getOwnerComponent().getRouter().getRoute("addColleagues")
                .attachMatched(this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("maddColleagues")
                .attachMatched(this._onRouteMatched, this);
        },

        _onRouteMatched: function (oEvt) {
            var sPenrMemId = this.getOwnerComponent().getModel("AppData").getProperty("/CurrentPernrMemId"),
                oEmployeesDataDef = new $.Deferred();

            this.getView().setBusy(true);
            this.sAppraisalId = oEvt.getParameter("arguments").AppraisalId;
            this._setViewAvailability(oEvt.getParameter("arguments").IndicatorId);

            DataAccess.getAddedEmployees(this.sAppraisalId, sPenrMemId)
                .then(this._parseBackendResponse.bind(this, oEmployeesDataDef));

            oEmployeesDataDef.then(function (mEmployeeData) {
                if (mEmployeeData.LOCKED) sap.m.MessageBox.warning(mEmployeeData.LOC_MSG);
            });
        },

        _parseBackendResponse: function(oEmployeesDataDef, oEmployees){
            var oColleaguesMapped = {},
                oViewData = this.getView().getModel("viewData"),
                sHdrInstructionText = DataAccess.getInstrText.call(this, "AD", "HEADER", oEmployees.ID_EVAL_PROC),
                sInstructionText = DataAccess.getInstrText.call(this, "AD", "BTN_POPUP", oEmployees.ID_EVAL_PROC);

            oViewData.setProperty("/isApprover", oEmployees.IS_APPROVER);
            oViewData.setProperty("/IdEvalProc", oEmployees.ID_EVAL_PROC);
            oViewData.setProperty("/CollApprove", oEmployees.COLL_APPROVE);
            oViewData.setProperty("/AddInfoText", sHdrInstructionText);
            oViewData.setProperty("/InstructionButton", sInstructionText);

            $.each(oEmployees.EmplSetTableSet.results, function (iInd, oEmpl) {
                delete oEmpl.__metadata;
                oColleaguesMapped[oEmpl.PERNR] = oEmpl;
            });

            this.getView().getModel("AddedColleagues").setData(oColleaguesMapped);

            var headerData = $.extend(true, {}, oEmployees);
            delete headerData.EmplSetTableSet;
            this.getView().setModel(new JSONModel(headerData), "Person");

            this._createCollegNumHTML(parseInt(oEmployees.COLL_MIN), parseInt(oEmployees.COLL_MAX));

            oEmployeesDataDef.resolve(headerData);
            this.getView().setBusy(false);
        },

        _setViewAvailability: function (sTaskStatus) {
            var bActionsEnabled = false;
            if (sTaskStatus) {
                bActionsEnabled = Config.taskStatusClosed.indexOf(sTaskStatus) > -1 ? false : true;
            }
            this.getView().getModel("viewData").setProperty("/ViewActionsEnabled", bActionsEnabled);
        },

        _createCollegNumHTML: function (iMin, iMax) {
            var collNumText = "Выберите ### коллег для участия в оценке",
                sReplaceText = "";
            if (iMin) {
                sReplaceText = " от " + "<span class='zBlueTextBold'>" + iMin + "</span>";
            }
            if (iMax) {
                sReplaceText += " до " + "<span class='zBlueTextBold'>" + iMax + "</span>";
            }
            this.getView().getModel("viewData").setProperty("/ColleaguesNum", collNumText.replace('###', sReplaceText));
        },

        onDeleteAdded: function (oEvt) {
            var oBindingContext = oEvt.getParameter("employeeItem").getBindingContext("AddedColleagues");
            var sPernr = oBindingContext.getObject().PERNR;
            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var oAddedColleaguesModel = this.getView().getModel("AddedColleagues");
            var oDeletedColleaguesModel = this.getView().getModel("DeletedColleagues");
            var oAddedColleagues = $.extend(true, {}, oAddedColleaguesModel.getData());
            var oDeletedColleagues = oDeletedColleaguesModel.getData();

            // show confirmation dialog
            MessageBox.show(oBundle.getText("ColleaguesDeleteDialogMsg"), {
                title: oBundle.getText("ColleaguesDeleteDialogTitle"),
                styleClass: "ZHCM_PM_0650 zCustomMessageBox",
                actions: [
                    MessageBox.Action.DELETE,
                    MessageBox.Action.CANCEL
                ],
                onClose: function (oAction) {
                    if (oAction !== MessageBox.Action.DELETE) {
                        return;
                    }

                    delete oAddedColleagues[sPernr];
                    oDeletedColleagues[sPernr] = true;

                    // update model
                    oAddedColleaguesModel.setProperty("/", $.extend(true, {}, oAddedColleagues));
                }
            });
        },

        onSearchColleagues: function (oEvt) {
            var oSearchField,
                oMultiInput;

            if (!this._oSearchEmployeeDialog) {
                this._oSearchEmployeeDialog = sap.ui.xmlfragment("SEDid", "zhcm_Hap_650.fragments.SearchEmployeeDialog", this);
                this._oSearchEmployeeDialog.addStyleClass("ZHCM_PM_0650");
                if (!this.getIsPhone()) {
                    sap.ui.core.Fragment
                        .byId("SEDid", "idSearchEmployeeDialogCloseButton")
                        .addStyleClass("zhcm_Hap_650CloseBtnDesktop");
                }
                this.getView().addDependent(this._oSearchEmployeeDialog);
            }
            this.getView().getModel("SearchResults").setData(null);
            this._addedViaDialogNum = 0;
            this._sSelectedORGEH = null;

            oMultiInput = sap.ui.core.Fragment.byId("SEDid", "minp_shlp_orgeh");
            oSearchField = sap.ui.core.Fragment.byId("SEDid", "fioSearchField");
            oSearchField.setValue("");
            oMultiInput.setValue("");

            // необходимо снимать фокус с элементов ввода при открытии диалога
            // ждем 1 кадр, чтобы появился html контрола
            $.sap.delayedCall(0, null, function () {
                oSearchField.$().find("input").get(0).setAttribute("tabindex", "-1");
                oMultiInput.$().find("input").get(0).setAttribute("tabindex", "-1");

                // почему-то не работает, видимо фокус на серч филд выставляется сильно позже открытия.
                // oCloseButton.focus();
            });

            this._oSearchEmployeeDialog.open();
        },

        handleSearch: function (oEvt) {
            var regEx1 = /^[ \t]+/g, // убирает пробелы в начале
                regex2 = /\s\s+/g, // неск. пробелов в 1
                sSearchString = oEvt.getParameter("query").replace(regEx1, "").replace(regex2, " "),
                that = this,
                sORGEH = this._sSelectedORGEH; // заполняется при закрытии СП по оргединицам

            if (sSearchString) {
                this._oSearchEmployeeDialog.setBusy(true);
                DataAccess.searchForEmpl(sSearchString, this.sAppraisalId, sORGEH).then(
                    function (aEmployees) {
                        that.getView().getModel("SearchResults").setData(aEmployees);
                        // Устанавливаем стиль для строки таблицы в зависимости от стажа сотрудника
                        that._setTabRowsDisabled();
                        that._oSearchEmployeeDialog.setBusy(false);
                    },
                    function (sError) {
                        that.getView().getModel("SearchResults").setData(null);
                        that._oSearchEmployeeDialog.setBusy(false);
                        MessageBox.error(sError);
                    }
                );
            }
        },

        handleSelect: function (oEvt) {
            var oContext = oEvt.getSource().getBindingContext("SearchResults"),
                oSelectedEmployee = oContext.getObject();

            this._addToColleaguesList(oSelectedEmployee);
        },

        _addToColleaguesList: function (oColleague) {
            var oAddedColleaguesModel = this.getView().getModel("AddedColleagues");
            var oDeletedColleaguesModel = this.getView().getModel("DeletedColleagues");
            var oAddedColleagues = $.extend(true, {}, oAddedColleaguesModel.getData());
            var oDeletedColleagues = $.extend(true, {}, oDeletedColleaguesModel.getData());
            var bDeletedOnFront = !!oDeletedColleagues[oColleague.PERNR];
            var oAppraisee = this.getView().getModel("Person").getData(); // Оцениваемый
            var oColleagueCopy = $.extend(true, {}, oColleague);
            var sErrorMessage = "Этот сотрудник уже участвует в оценке";

            // Если коллега уже добавлен - не добавляем
            if (oAddedColleagues[oColleagueCopy.PERNR]) {
                MessageToast.show(sErrorMessage);
                return false;
            }
            
            if (oColleagueCopy.DISABLED && !bDeletedOnFront) {
                if (oColleagueCopy.DISABLED_MESSAGE) {
                    sErrorMessage = oColleagueCopy.DISABLED_MESSAGE;
                }
                MessageToast.show(sErrorMessage);
                return false;
            }

            // Проверка на соответветствие мнимальному стажу выбранного сотрудника
            if (Number(oColleagueCopy.DATE) < Number(oAppraisee.MIN_EXP)) {
                MessageToast.show("Стаж сотрудника менее необходимого количества дней: " 
                    + Number(oAppraisee.MIN_EXP) + ". Недоступен для выбора.");
                return false;
            }

            // Добавляем коллегу
            delete oColleagueCopy.__metadata;
            delete oColleagueCopy.APPRAISALID;
            delete oColleagueCopy.DATE;
            delete oColleagueCopy.DISABLED;
            delete oColleagueCopy.DISABLED_MESSAGE;

            oAddedColleagues[oColleagueCopy.PERNR] = oColleagueCopy;

            if (bDeletedOnFront) {
                delete oDeletedColleagues[oColleagueCopy.PERNR];
            }

            //update model
            oAddedColleaguesModel.setProperty("/", $.extend(true, {}, oAddedColleagues));
            oAddedColleaguesModel.refresh(true);
            this._addedViaDialogNum += 1;

            this._setTabRowsDisabled();
            MessageToast.show("Добавлен");

            return true;
        },

        handleClose: function (oEvt) {
            var that = this;
            if (!this._addedViaDialogNum) {
                MessageBox.show("Коллеги не выбраны. Вы уверены, что хотите закрыть поиск?", {
                    title: "Закрыть",
                    styleClass: "ZHCM_PM_0650 zCustomMessageBox",
                    actions: [MessageBox.Action.YES, MessageBox.Action.CANCEL],
                    onClose: function (oAction) {
                        if (oAction === MessageBox.Action.YES) {
                            that._oSearchEmployeeDialog.close();
                            that.getView().setBusy(false);
                        }
                    }
                });
            } else {
                this._oSearchEmployeeDialog.close();
                this.getView().setBusy(false);
            }
        },

        onSaveList: function (oEvt) {
            this._sendList("");
        },

        onSendToApprove: function (oEvt) {
            this._sendList("SEND");
        },

        onSendNoApprove: function (oEvt) {
            this._sendList("COMPLETE");
        },

        onApprove: function (oEvt) {
            this._sendList("APPR");
        },

        onBtnBackPress: function (oEvt) {
            if (this.getOwnerComponent().getModel("device").getProperty('/isPhone')) {
                Utilits.navBack(this.getView(), true);
            } else {
                Utilits.navBack(this.getView(), false);
            }
        },

        _sendList: function (sAction) {
            this.getView().setBusy(true);
            var that = this;
            var aAdded = $.extend(true, {}, this.getView().getModel("AddedColleagues").getData());
            var oDeletedColleaguesModel = this.getView().getModel("DeletedColleagues");
            var aAddedParsed = Object.keys(aAdded).map(function (sKey) {
                return aAdded[sKey];
            });

            var sendData = {
                APPRAISAL_ID: this.sAppraisalId,
                BUTTON: sAction,
                EmplSetTableSet: aAddedParsed
            };

            DataAccess.sendEmployeeList(sendData).then(
                function (oRes) {
                    that.getView().setBusy(false);
                    if (sendData.BUTTON != "") {
                        if (that.getOwnerComponent().getModel("device").getProperty('/isPhone')) {
                            that.getOwnerComponent().getRouter().navTo("mOverview");
                        } else {
                            that.getOwnerComponent().getRouter().navTo("overview");
                        }
                    }
                    oDeletedColleaguesModel.setData({});
                    MessageToast.show("Сохранено");
                },
                function (sError) {
                    that.getView().setBusy(false);
                    MessageBox.error(sError);
                }
            );
        },

        handleValueHelpORGEH: function (oEvt) {
            if (!this._oValueHelpDialog) {
                this._oValueHelpDialog = sap.ui.xmlfragment("idOrgehDialog", "zhcm_Hap_650.fragments.SearchOrgehDialog", this);
                this._oValueHelpDialog.addStyleClass("ZHCM_PM_0650");
                this.getView().addDependent(this._oValueHelpDialog);
            }

            var oOrgDialog = sap.ui.core.Fragment.byId("idOrgehDialog", "sld_ogreh"),
                oBinding = oOrgDialog.getBinding("items"),
                that = this;

            // Очищаем фильтр в средстве поиска оргедниц
            oBinding.filter([]);

            oOrgDialog._getCancelButton().addStyleClass("zhcm_Hap_650CloseBtn");
            if (!this.getIsPhone()) {
                oOrgDialog._getCancelButton().addStyleClass("zhcm_Hap_650CloseBtnDesktop");
            }
            oOrgDialog._getCancelButton().setType("Unstyled");
            this._oSearchEmployeeDialog.setBusy(true);

            DataAccess.get_sf_orgeh(this.sAppraisalId).then(
                function (aOrgeh) {
                    that.getView().getModel("OrgehValueHelpSet").setData(aOrgeh);
                    that._oSearchEmployeeDialog.setBusy(false);
                    that._oValueHelpDialog.open();
                },
                function (sError) {
                    MessageBox.error(sError);
                    that._oSearchEmployeeDialog.setBusy(false);
                }
            );
        },

        handleSearchORGEH: function (oEvt) {
            var sValue = oEvt.getParameter("value"),
                oFilter = new Filter("ORGEHTXT", sap.ui.model.FilterOperator.Contains, sValue),
                oBinding = oEvt.getSource().getBinding("items");

            oBinding.filter([oFilter]);
        },

        handleConfirmORGEH: function (oEvt) {
            var oSelectedItem = oEvt.getParameter("selectedItem"),
                sFioInput = sap.ui.core.Fragment.byId("SEDid", "fioSearchField").getValue(),
                that = this;

            if (oSelectedItem) {
                var sORGEHTXT = oSelectedItem.getTitle(),
                    sORGEH = oSelectedItem.getDescription();
                this._sSelectedORGEH = sORGEH; // заполняется для использования в методе handleSearch

                var oInputOrgeh = sap.ui.core.Fragment.byId("SEDid", "minp_shlp_orgeh");
                oInputOrgeh.setValue(oSelectedItem.mProperties.title)

                if (sFioInput) {
                    this._oSearchEmployeeDialog.setBusy(true);
                    DataAccess.searchForEmpl(sFioInput, this.sAppraisalId, sORGEH).then(
                        function (aEmployees) {
                            that.getView().getModel("SearchResults").setData(aEmployees);
                            // Устанавливаем стиль для строки таблицы в зависимости от стажа сотрудника
                            that._setTabRowsDisabled();
                            that.getView().setBusy(false);
                            that._oSearchEmployeeDialog.setBusy(false);
                        },
                        function (sError) {
                            that.getView().getModel("SearchResults").setData(null);
                            that._oSearchEmployeeDialog.setBusy(false);
                            MessageBox.error(sError);
                        }
                    );


                    // в ИЕ при очень длинном тексте токена диалог уезжает влево за край экрана. Чиним.
                    $.sap.delayedCall(0, null, function () {
                        that._oSearchEmployeeDialog.rerender();
                    });
                }                
            }
        },

        handleClouseORGEH: function (oEvt) {
        },

        _setTabRowsDisabled: function (aEmployees) {
            var oAppraisee = this.getView().getModel("Person").getData(),
                oTabResults = sap.ui.core.Fragment.byId("SEDid", "tblColleaguesResults"),
                aTabItems = oTabResults.getItems(),
                oAddedColleagues = this.getView().getModel("AddedColleagues").getData(),
                oDeletedColleagues = this.getView().getModel("DeletedColleagues").getData();

            if (! aEmployees) {
                aEmployees = this.getView().getModel("SearchResults").getData();
            }

            // Устанавливаем стиль для строки таблицы в зависимости от стажа сотрудника
            aEmployees.forEach(function (oEmployee, i) {
                // Проверка на соответветствие мнимальному стажу выбранного сотрудника
                // или - коллега уже добавлен, но не сохранен. Тоже серым
                if (Number(oEmployee.DATE) >= Number(oAppraisee.MIN_EXP) &&
                    !oAddedColleagues[oEmployee.PERNR] &&
                    (!oEmployee.DISABLED || oDeletedColleagues[oEmployee.PERNR])) {
                    aTabItems[i].removeStyleClass("ZHCM_PM_0650").removeStyleClass("zhcm_Hap_650DisableText");
                    aTabItems[i].addStyleClass("ZHCM_PM_0650").addStyleClass("zhcm_Hap_650EnableText");
                }
                else {
                    aTabItems[i].removeStyleClass("ZHCM_PM_0650").removeStyleClass("zhcm_Hap_650EnableText");
                    aTabItems[i].addStyleClass("ZHCM_PM_0650").addStyleClass("zhcm_Hap_650DisableText");
                }
            });
        },

        onTokenUpdate_minp_shlp_orgeh: function (oEvt) {
            if (oEvt.getParameter("type") === Tokenizer.TokenUpdateType.Removed) {
                var sSF_FIO = sap.ui.core.Fragment.byId("SEDid", "fioSearchField").getValue(),
                    that = this;

                this._sSelectedORGEH = ""; // заполняется для использования в методе handleSearch
                if (sSF_FIO) {
                    this._oSearchEmployeeDialog.setBusy(false);
                    DataAccess.searchForEmpl(sSF_FIO, this.sAppraisalId, null).then(
                        function (aEmployees) {
                            that.getView().getModel("SearchResults").setData(aEmployees);
                            // Устанавливаем стиль для строки таблицы в зависимости от стажа сотрудника
                            that._setTabRowsDisabled();
                            that._oSearchEmployeeDialog.setBusy(false);
                        },
                        function (sError) {
                            that.getView().getModel("SearchResults").setData(null);
                            that._oSearchEmployeeDialog.setBusy(false);
                            MessageBox.error(sError);
                        }
                    );
                }
            }
        },

        onToggleHeader: function () {
            var oViewData = this.getView().getModel("viewData");
            oViewData.setProperty("/HeaderExpanded", !oViewData.getProperty("/HeaderExpanded"));
        },

        onItemsBindingChangeColleaguesGrid: function (oEvent) {
            var oColleaguesGrid = this.getView().byId("colleaguesGrid");
            var aItems = oColleaguesGrid.getItems();

            if (aItems.length > 0) {
                // ждем 1 кадр, пока карточки появятся в DOM
                $.sap.delayedCall(0, null, function () {
                    // удаляем результат старого выставления ширины
                    aItems.forEach(function (oItem) {
                        oItem.$().css("display", "block");
                        oItem.$().height("auto");
                    });

                    $.sap.delayedCall(0, null, function () {
                        aItems.forEach(function (oItem) {
                            oItem.$().css("display", "flex");
                        });

                        // вычисляем высоту самой высокой карточки
                        var iMaxHeight = Math.max.apply(null, aItems.map(function (oItem) {
                            return oItem.getDomRef().clientHeight;
                        }));

                        // В эксплорере по неизвестной причине появляется лишнее пространство
                        // между текстом и кнопкой компенсируем
                        // задаем ее всем карточкам

                        aItems.forEach(function (oItem) {
                            oItem.$().height(iMaxHeight);
                        });
                    });
                });
            }
        },

        
    });
});