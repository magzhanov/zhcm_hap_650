sap.ui.define([
    "zhcm_Hap_650/Controller/Survey.controller",
    "sap/ui/model/json/JSONModel",
    "../utils/Formatter"
], function (baseController, JSONModel, Formatter) {
    "use strict";

    return baseController.extend("zhcm_Hap_650.Controller.mSurvey", {
        formatter: Formatter,

        onInit: function () {
            this.getOwnerComponent().getRouter().getRoute("msurvey")
                .attachMatched(this._onRouteMatched, this);
            this._initCommonModels();
            this._switchBetweenCompsListAndSurvey();
        },

        _onRouteMatchedForTesting: function () {
            var oTestModel = new JSONModel();
            this._parseSurveyData(oTestModel.getData(), true);
        },

        _bindResponseParser: function() {
            return this._parseSurveyData.bind(this, false);
        },

        onCompetencePress: function (oEvt) {            
            this.getView().getModel("viewData").getData().CompetenceIsPress = true;
            this._switchBetweenCompsListAndSurvey();
            this._removeCompsErrorState();
            this._composeUIForCompetencyAppraisal(oEvt.getSource().getBindingContext("SurveyData"));
            this.triggerCompIconFormatter();
        },

        onGoBackToCompsList: function (oEvt) {
            this.getView().getModel("viewData").getData().CompetenceIsPress = false;
            this.getView().getModel("SurveyData").refresh(true);
            this._switchBetweenCompsListAndSurvey();
        }
    });
});


