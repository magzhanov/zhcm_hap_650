sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "./BaseController",
    "../Utils/Config",
    "../Utils/DataAccess",
    "../Utils/Utilits",
    "../Utils/Formatter",
    "sap/m/MessageBox",
    "sap/m/Popover",
    "sap/m/MessagePopover",
    "sap/m/MessagePopoverItem",
    "sap/ui/model/CompositeType",
    "sap/ui/model/SimpleType",
    "../CustomControls/ZClickableText"
], function (JSONModel, BaseController, Config, DataAccess, Utilits, Formatter, MessageBox, Popover,
             MessagePopover, MessagePopoverItem, CompositeType, SimpleType, ZClickableText) {
    "use strict";

    var oSurveyController = BaseController.extend("zhcm_Hap_650.Controller.Survey", {
        formatter: Formatter,

        _MessagePopover: new MessagePopover({
            items: {
                path: "/",
                template: new MessagePopoverItem({
                    type: "{type}",
                    title: "{title}"
                })
            }
        }),

        _IndicatorsFragments: {},
        _CompetenceFragments: {},
        _CommentsFragment: null,

        onInit: function () {
            console.log('BEGIN Survey.onInit');
            // #DEBUG START
            // if (window.location.hostname.toLowerCase() === "sms00990" ||
            //     window.location.hostname === "127.0.0.1" ||
            //     window.location.hostname.toLowerCase() === "localhost")
            window.goControllerSu = this;
            // #DEBUG END

            this.getOwnerComponent().getRouter().getRoute("survey").attachMatched(this._onRouteMatched, this);
            this._initCommonModels();

            var oEventBus = sap.ui.getCore().getEventBus();
            oEventBus.subscribe(
                "isCompRatingCompleted",    //канал
                "iconComplete",             //название события
                this._onIconComplete,       //вызываем метод _onIconComplete в данном (this) контроллере
                this
            );
            console.log('END Survey.onInit');
        },

        //метод, который будет вызываться, при отправке события из компонента
        _onIconComplete: function (channel, event, item) {
            console.log('BEGIN Survey._onIconComplete');
            if (item) {
                var selector = '[data-compid="' + item.COMP_ID + '"]',
                    oCompUICtrl = $(selector);

                if (oCompUICtrl) {
                    selector = "#" + oCompUICtrl.attr("Id");
                    var bFlag = $(selector).hasClass("zCompWithError");
                    if (bFlag) {
                        var aCompListItems = this.getView().byId("competenciesList").getItems();
                        if (aCompListItems) {
                            for (var i = 0; i < aCompListItems.length; i++) {
                                var sCompId = aCompListItems[i].data().compId;
                                if (item.COMP_ID === sCompId) {
                                    aCompListItems[i].removeStyleClass("zCompWithError");
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            console.log('END Survey._onIconComplete');
        },

        _initCommonModels: function() {
            console.log('BEGIN Survey._initCommonModels');
            this.getView().setModel(new JSONModel({
                HeaderExpanded: true,
                true: true,
                false: false,
                CompetenceIsPress: false,
                mOverviewNow: true
            }), "viewData");
            this.getView().setModel(new JSONModel(), "SurveyData");
            this._MessagePopover.setModel(new JSONModel());
            console.log('END Survey._initCommonModels');
        },

        _onRouteMatched: function (oEvt) {
            console.log('BEGIN Survey._onRouteMatched');
            var oArgs = oEvt.getParameter("arguments"),
                that = this,
                sPenrMemId = this._getAppData("/CurrentPernrMemId");

            this._MessagePopover.getModel().setData();
            this.getView().getModel("viewData").setProperty("/msgNum", 0);
            this.getView().setBusy(true);
            this._cleanUp();

            if (sPenrMemId) {
                DataAccess.getSurveyData(oArgs.AppraisalId, sPenrMemId, this._bindResponseParser());
            } else {
                DataAccess.getUserMemId().then(
                    function (oMemIds) {
                        return  DataAccess.getSurveyData(
                                    oArgs.AppraisalId, 
                                    oMemIds[0].PERNR_MEMID, 
                                    this._bindResponseParser()
                                );
                    }.bind(this),
                    function (oError) {
                        that.oView.setBusy(false);
                    }
                );
            }
            console.log('END Survey._onRouteMatched');
        },

        _bindResponseParser: function() {
            console.log('BEGIN Survey._bindResponseParser');
          return this._parseSurveyData.bind(this, true);
            console.log('END Survey._bindResponseParser');
        },

        // accepts any amount of boolean arguments, if any is false - sets false
        _setViewAvailability: function () {
            console.log('BEGIN Survey._setViewAvailability');
            var bActionsEnabled = !!Math.min.apply(null, arguments);
            this.getView().getModel("viewData").setProperty("/ViewActionsEnabled", bActionsEnabled);
            console.log('END Survey._setViewAvailability');
        },

        _isViewAvailableByStatus: function (sTaskStatus) {
            return Config.taskStatusClosed.indexOf(sTaskStatus) === -1;
        },

        _getAppData: function (sProperty) {
            return this.getOwnerComponent().getModel("AppData").getProperty(sProperty);
        },

        _setAppData: function (sPropName, Value) {
            this.getOwnerComponent().getModel("AppData").setProperty(sPropName, Value);
        },

        _parseSurveyData: function (bOpenFirstItem, oData) {
            console.log('BEGIN Survey._parseSurveyData');
            var headerData = $.extend({}, oData),
                that = this,
                oSurveyModel = this.getView().getModel("SurveyData"),
                oViewData = this.getView().getModel("viewData");
            this.getView().setModel(new JSONModel(headerData), "Person");

            if (oData.LOCKED) sap.m.MessageBox.warning(oData.LOC_MSG);
            var sHdrInstructionText = DataAccess.getInstrText.call(this, "SU", "HEADER", oData.ID_EVAL_PROC);
            var sInstructionText = DataAccess.getInstrText.call(this, "SU", "BTN_POPUP", oData.ID_EVAL_PROC);

            oViewData.setProperty("/AddInfoText", sHdrInstructionText);
            oViewData.setProperty("/InstructionButton", sInstructionText);
            oViewData.setProperty("/IdEvalProc", oData.ID_EVAL_PROC);
            this._setAppData("/NameEvalProc", oData.NAME_EVAL_PROC);
            this._setAppData("/TaskTxt", oData.TASK_TXT);
            this._setViewAvailability(this._isViewAvailableByStatus(oData.INDICATOR_ID));

            var fnCompare = function(a, b) {
                if (!b) {
                    return -1;
                }
                if (!a) {
                    return 1;
                }

                var aa = parseInt(a.SORTING_ORDER, 10);
                var bb = parseInt(b.SORTING_ORDER, 10);

                if (aa < bb) {
                    return -1;
                }
                if (aa > bb) {
                    return 1;
                }
                return 0;
            };

            var mSurveyData = oData;
            mSurveyData.Competencies = oData.Competencies.results || oData.Competencies;
            mSurveyData.CompetencyScaleValues = oData.CompetencyScaleValues.results || oData.CompetencyScaleValues;

            if (mSurveyData.Competencies) {
                mSurveyData.Competencies.sort(fnCompare);
            }

            $.each(mSurveyData.Competencies, function (iInd, Competency) {
                Competency.MARK = +Competency.MARK || 0;
                if (mSurveyData.CompetencyScale.DATA_TYPE === "Q") {
                    Competency._MARK_TO_SHOW = Competency.MARK && mSurveyData.CompetencyScaleValues[Competency.MARK-1].VALUE;
                }
                Competency.Indicators = Competency.Indicators.results || Competency.Indicators;
                if (Competency.Indicators) {
                    Competency.Indicators.sort(fnCompare);
                }

                $.each(Competency.Indicators, function (iInd, Indic) {
                    Indic.M_VALUE = +Indic.M_VALUE;
                });
                if (mSurveyData.IN_AVERAGE) {
                    Competency._AverageVal = that._calcAverage(Competency, mSurveyData.IndicatorScale.DATA_TYPE, mSurveyData.IndicatorScaleValues.results);
                }
            });

            mSurveyData.IndicatorScaleValues = oData.IndicatorScaleValues.results || oData.IndicatorScaleValues;
            mSurveyData.DescriptionScaleValues = oData.DescriptionScaleValues.results || oData.DescriptionScaleValues;
            mSurveyData.DescriptionCompScaleValues = oData.DescriptionCompScaleValues.results || oData.DescriptionCompScaleValues;
            mSurveyData.CompetencyScale.MIN = parseFloat(mSurveyData.CompetencyScale.MIN);
            mSurveyData.CompetencyScale.MAX = parseFloat(mSurveyData.CompetencyScale.MAX);
            mSurveyData.CompetencyScale.STEP = parseFloat(mSurveyData.CompetencyScale.STEP);
            mSurveyData.IndicatorScale.MIN = parseFloat(mSurveyData.IndicatorScale.MIN);
            mSurveyData.IndicatorScale.MAX = parseFloat(mSurveyData.IndicatorScale.MAX);
            mSurveyData.IndicatorScale.STEP = parseFloat(mSurveyData.IndicatorScale.STEP);

            oSurveyModel.setData(mSurveyData);

            if (bOpenFirstItem) {
                var oList = this.getView().byId("competenciesList");
                var firstItem = oList.getItems()[0];
                if (firstItem) {
                    oList.setSelectedItem(firstItem);
                    oList.fireItemPress();
                }
            }

            this._removeCompsErrorState();
            this.getView().setBusy(false);
            console.log('END Survey._parseSurveyData');
        },

        onCalcAverageByIndicators: function (oEvt) {
            console.log('BEGIN Survey.onCalcAverageByIndicators');
            var s = oEvt.getParameter("rootControl"), // slider
                sIndicPath = s.getBinding("scale").getContext().getPath(),
                sCompPath = sIndicPath.split("Indicators")[0],
                oComp = this.getView().getModel("SurveyData").getProperty(sCompPath),
                sAvrgPath = sCompPath + "_AverageVal";

            $.sap.delayedCall(0, this, function () {
                this.getView().getModel("SurveyData").setProperty(sAvrgPath, this._calcAverage(oComp));
                //this.triggerCompIconFormatter();
            });
            console.log('END Survey.onCalcAverageByIndicators');
        },

        _calcAverage: function (oComp, sScaleDataType, aIndicatorScaleValues) {
            console.log('BEGIN Survey._calcAverage');

            var iIndicatorsWithRating = 0,
                fCompRating = 0,
                oSurveyModel = this.getView().getModel("SurveyData");

            aIndicatorScaleValues = aIndicatorScaleValues || oSurveyModel.getProperty("/IndicatorScaleValues"),
            sScaleDataType = sScaleDataType || oSurveyModel.getProperty("/IndicatorScale/DATA_TYPE");

            $.each(oComp.Indicators, function (sInd, oIndic) {
                if (oIndic.CANT_ESTIMATE === true ||
                    oIndic.NO_VALUE === true) {
                    return;
                }
                if (sScaleDataType === "Q" && +oIndic.Q_VALUE) {
                    fCompRating += +aIndicatorScaleValues[oIndic.Q_VALUE-1].VALUE;
                    iIndicatorsWithRating += 1;
                }
                if (sScaleDataType === "M") {
                    fCompRating += Number(oIndic.M_VALUE);
                    iIndicatorsWithRating += 1;
                }
            });
            console.log('END Survey._calcAverage');
            return iIndicatorsWithRating ? fCompRating / iIndicatorsWithRating : 0;
        },

        onItemPress: function (oEvt) {
            var oCompContext = oEvt.getSource().getSelectedItem().getBindingContext("SurveyData");
            this._composeUIForCompetencyAppraisal(oCompContext);
            this.triggerCompIconFormatter();
        },

        _cleanUp: function () {
            console.log('BEGIN Survey._cleanUp');
            this.getView().byId("IndicatorsPanel").removeAllContent();
            console.log('END Survey._cleanUp');
        },


        _composeUIForCompetencyAppraisal: function (oCompContext) {
            console.log('BEGIN Survey._composeUIForCompetencyAppraisal');
            var sCompetencyPath = "SurveyData>" + oCompContext.getPath(),
                oPanel = this.getView().byId("IndicatorsPanel");

            oPanel.destroyContent();

            var oFragmentInd = this._getIndicatorsFragment(oCompContext),
                oFragmentComp = this._getCompetenceFragment(oCompContext),
                oFragmentComments = this._getCommentsFragment(),
                oCompDescr = new sap.m.Text({
                    text: "{SurveyData>DESCRIPTION}"
                }).addStyleClass("zDescription");

            this._adjustDataBeforeSurveyComposition();
            this._setTexts(oCompContext);

            oPanel.bindElement(sCompetencyPath);

            oPanel.addContent(oCompDescr);
            if (this._hasIndicatorsToShow(oCompContext)) {
                oPanel.addContent(oFragmentInd);
            }
            if (!this.getView().getModel("SurveyData").getProperty("/IN_AVERAGE")) {
                oPanel.addContent(oFragmentComp);
            }
            oPanel.addContent(oFragmentComments);
            console.log('END Survey._composeUIForCompetencyAppraisal');
        },

        _getIndicatorsFragment: function (oCompContext) {
            console.log('BEGIN Survey._getIndicatorsFragment');
            var sScale = oCompContext.getModel().getProperty("/IndicatorScale/SCALE_TYPE");
            var sFragmentsSectionName = this.getIsPhone()
                    ? "indicatorsFragmentMobile"
                    : "indicatorsFragmentDesktop";
            var sFragmentName = Config[sFragmentsSectionName][sScale];
            var oClone;
            if (!this._IndicatorsFragments[sScale]) {
                this._IndicatorsFragments[sScale] = sap.ui.xmlfragment(sFragmentName, this);
                this.getView().addDependent(this._IndicatorsFragments[sScale]);
            }

            oClone = this._IndicatorsFragments[sScale].clone();
            console.log('END Survey._getIndicatorsFragment');
            return oClone;
        },

        _getCompetenceFragment: function (oCompContext) {
            console.log('BEGIN Survey._getCompetenceFragment');
            var sScale = oCompContext.getModel().getProperty("/CompetencyScale/SCALE_TYPE");
            var sFragmentsSectionName = this.getIsPhone()
                    ? "competenceFragmentMobile"
                    : "competenceFragmentDesktop";
            var sFragmentName = Config[sFragmentsSectionName][sScale];
            var oClone;
            if (!this._CompetenceFragments[sScale]) {
                this._CompetenceFragments[sScale] = sap.ui.xmlfragment(sFragmentName, this);
                this.getView().addDependent(this._CompetenceFragments[sScale]);
            }
            oClone = this._CompetenceFragments[sScale].clone();
            console.log('END Survey._getCompetenceFragment');
            return oClone;
        },

        _getCommentsFragment: function () {
            console.log('BEGIN Survey._getCommentsFragment');
            var sFragmentName = "zhcm_Hap_650.fragments.SurveyComments";
            if (!this._CommentsFragment) {
                this._CommentsFragment = sap.ui.xmlfragment(sFragmentName, this);
                this.getView().addDependent(this._CommentsFragment);
            }
            console.log('END Survey._getCommentsFragment');
            return this._CommentsFragment.clone();
        },

        _hasIndicatorsToShow: function (oCompContext) {
            console.log('BEGIN Survey._hasIndicatorsToShow');
            var sIndicPath = oCompContext.getPath() + "/Indicators",
                aIndic = oCompContext.getModel("SurveyData").getProperty(sIndicPath),
                bHasIndicators = true;
            if (aIndic.length == 0) {
                bHasIndicators = false;
            }
            console.log('END Survey._hasIndicatorsToShow');
            return bHasIndicators;
        },

        _setTexts: function (oCompContext) {
            console.log('BEGIN Survey._setTexts');
            var oCompParams = oCompContext.getModel().getProperty(oCompContext.getPath());
            this.getView().byId("iCompetencyName").setText(oCompParams.NAME);
            this.getView().byId("i0pName").setText(oCompParams.BLOCK_NAME);
            console.log('END Survey._setTexts');
        },

        _adjustDataBeforeSurveyComposition: function (sIndScaleType) {
            console.log('BEGIN Survey._adjustDataBeforeSurveyComposition');
            sIndScaleType = sIndScaleType || Config.indicatorsFragmentDesktop.Table;
            if (sIndScaleType === Config.indicatorsFragmentDesktop.Table) {
                var aIndScaleVals = this.getView().getModel("SurveyData").getProperty("/IndicatorScaleValues/"),
                    oColumnsModel = new JSONModel(),
                    aColumns = $.extend([], aIndScaleVals);

                aColumns.unshift({DESCRIPTION: "Индикаторы"});

                if (this.getView().getModel("SurveyData").getProperty("/INDIC_REJECT")) {
                    aColumns.push({DESCRIPTION: "Не могу оценить"});
                }

                oColumnsModel.setData(aColumns);
                this.getView().setModel(oColumnsModel, "IndicatorsTableColumns");
            }
            console.log('END Survey._adjustDataBeforeSurveyComposition');
        },

        onCompetencySliderCantEstimate: function (oEvt) {
            console.log('BEGIN Survey.onCompetencySliderCantEstimate');

            var that = this;
            var oContext = oEvt.getSource().getBindingContext("SurveyData");
            var oSurveyDataModel = this.getView().getModel("SurveyData");
            var bPressed = oEvt.getSource().getPressed();
            var sCompetencyPath = oContext.getPath();

            this.getView().getModel("viewData").setProperty("/JustPressedCantEstimate", oContext);
            oSurveyDataModel.setProperty(sCompetencyPath + "/NO_VALUE", true);
            this.triggerCompIconFormatter();
            $.sap.delayedCall(0, this, function () {
                oSurveyDataModel.setProperty(sCompetencyPath + "/_HIDE_INPUT_VALUE", bPressed);
            });
            console.log('END Survey.onCompetencySliderCantEstimate');
        },

        onIndicSliderCantEstimate: function (oEvt) {
            console.log('BEGIN Survey.onIndicSliderCantEstimate');

            var oContext = oEvt.getSource().getBindingContext("SurveyData");
            var oSurveyDataModel = this.getView().getModel("SurveyData");
            var bPressed = oEvt.getSource().getPressed();
            var sIndicatorPath = oContext.getPath();
            var sCompetencyPath = sIndicatorPath.split("/Indicators")[0];
            var oCompetency = oContext.getModel().getProperty(sCompetencyPath);

            this.getView().getModel("viewData").setProperty("/JustPressedCantEstimate", oContext);
            oSurveyDataModel.setProperty(sCompetencyPath + "/_AverageVal", this._calcAverage(oCompetency));
            oSurveyDataModel.setProperty(sIndicatorPath + "/NO_VALUE", bPressed);
            //this.triggerCompIconFormatter();
            $.sap.delayedCall(0, this, function () {
                oSurveyDataModel.setProperty(sIndicatorPath + "/_HIDE_INPUT_VALUE", bPressed);
            });
            console.log('END Survey.onIndicSliderCantEstimate');
        },

        _showDetail: function (oItem) {
            console.log('BEGIN Survey._showDetail');
            var bReplace = !Device.system.phone;
            // set the layout property of FCL control to show two columns
            this.getModel("viewData").setProperty("/layout", "TwoColumnsMidExpanded");
            this.getRouter().navTo("object", {
                objectId: oItem.getBindingContext().getProperty("ObjectID")
            }, bReplace);
            console.log('END Survey._showDetail');
        },

        indicatorsTableCellsFactory: function (sId, oContext) {
            console.log('BEGIN Survey.indicatorsTableCellsFactory');

            var aCells = [],
                that = this,
                bEnabled = this.getView().getModel("viewData").getProperty("/ViewActionsEnabled"),
                mCurrIndic = oContext.getModel().getProperty(oContext.sPath),
                oIndicDescr = new ZClickableText({
                        text: mCurrIndic.DESCRIPTION && mCurrIndic.DESCRIPTION.length > 0
                                ? mCurrIndic.DESCRIPTION
                                : mCurrIndic.NAME
                    })
                    .addStyleClass("zIndicatorsTableDescriptionText")
                    .attachPress(this.onClickTextIndicatorsTableDescription, this);

            aCells.push(oIndicDescr);

            $.each(this.getView().getModel("SurveyData").getProperty("/IndicatorScaleValues/"),
                function (sIndex, oScale) {
                    var tButton = new sap.m.ToggleButton({
                        text: +oScale.VALUE,
                        press: that.indicatorsTableButtonPressed.bind(that),
                        pressed: mCurrIndic.Q_VALUE === oScale.ID ? true : false,
                        enabled: bEnabled
                    });
                    tButton.addStyleClass("zhcm_Hap_650TableCellButton");
                    tButton.data("valToSet", oScale.ID);
                    aCells.push(tButton);
                });

            if (this.getView().getModel("SurveyData").getProperty("/INDIC_REJECT")) {
                var nButton = new sap.m.ToggleButton({
                        text: "N",
                        press: that.indicatorsTableButtonPressed.bind(that),
                        pressed: mCurrIndic.CANT_ESTIMATE ? true : false,
                        enabled: bEnabled
                    });
                nButton.addStyleClass("zhcm_Hap_650TableCellButton");
                nButton.data("valToSet", "N");
                aCells.push(nButton);
            }

            var oColumnListItem = new sap.m.ColumnListItem(sId, {cells: aCells});
            console.log('END Survey.indicatorsTableCellsFactory');
            return oColumnListItem;
        },

        indicatorsTableButtonPressed: function (oEvt) {
            console.log('BEGIN Survey.indicatorsTableButtonPressed');

            var bPressed = oEvt.getParameter("pressed"),
                sValToSet = bPressed ? oEvt.getSource().data().valToSet : "0000",
                that = this,
                aCells = oEvt.getSource().getParent().getCells();

            $.each(aCells, function (sIndex, oCell) {
                if (sIndex === 0) {
                    return;
                }
                if (oCell.sId != oEvt.getSource().sId) {
                    oCell.setPressed(false);
                }
                if (oCell.sId == oEvt.getSource().sId) {
                    var bCantEstimate = false;
                    if (sValToSet === "N") {
                        sValToSet = "0000";
                        bCantEstimate = true;
                    }
                    var oContext = oEvt.getSource().getParent().getBindingContext("SurveyData"),
                        sCantEstimatePath = oContext.getPath() + "/CANT_ESTIMATE",
                        sNoValuePath = oContext.getPath() + "/NO_VALUE",
                        sRatedPath = oContext.getPath() + "/Q_VALUE",
                        sCompPath = oContext.getPath().split("/Indicators")[0],
                        sAveragePath = sCompPath + "/_AverageVal";

                    oContext.getModel().setProperty(sRatedPath, sValToSet);
                    oContext.getModel().setProperty(sCantEstimatePath, bCantEstimate);
                    oContext.getModel().setProperty(sNoValuePath, !bPressed || bCantEstimate);
                    var fAverage = that._calcAverage(oContext.getModel().getProperty(sCompPath));
                    oContext.getModel().setProperty(sAveragePath, fAverage);
                }
            });

            this.triggerCompIconFormatter();
            console.log('END Survey.indicatorsTableButtonPressed');
        },

        onBtnBackPress: function (oEvt) {
            console.log('BEGIN Survey.onBtnBackPress');
            if (this.getOwnerComponent().getModel("device").getProperty("/isPhone")) {
                var CompetenceIsPress = this.getView().getModel("viewData").getData().CompetenceIsPress;
                if (CompetenceIsPress) {
                    this._switchBetweenCompsListAndSurvey();
                    this.getView().getModel("viewData").getData().CompetenceIsPress = false;
                } else {
                    Utilits.navBack(this.getView(), true)
                }
            } else {
                Utilits.navBack(this.getView(), false)
            }
            console.log('END Survey.onBtnBackPress');
        },

        _switchBetweenCompsListAndSurvey: function() {
            console.log('BEGIN Survey._switchBetweenCompsListAndSurvey');

            if (this.getOwnerComponent().getModel("device").getProperty("/isPhone") && 
                this.getView().getModel("viewData").getData().mOverviewNow) {
                var self = this;
                self.getView().getModel("viewData").getData().mOverviewNow = false;
                $(window).on("popstate", function(event) {
                    var CompetenceIsPress = self.getView().getModel("viewData").getData().CompetenceIsPress;
                    if (CompetenceIsPress) {
                        self._switchBetweenCompsListAndSurvey();
                        self.getView().getModel("viewData").getData().CompetenceIsPress = false;
                    } else {
                        if (!self.getView().getModel("viewData").getData().mOverviewNow) {
                            self.getOwnerComponent().getRouter().navTo("mOverview");
                            self.getView().getModel("viewData").getData().mOverviewNow = true;
                        }
                    }
                });
            }
            var bCurrentMode = this.getView().getModel("viewData").getProperty("/compsListVisible");
            this.getView().getModel("viewData").setProperty("/compsListVisible", !bCurrentMode);
            this.triggerCompIconFormatter();
            console.log('END Survey._switchBetweenCompsListAndSurvey');
        },

        _processSend: function (sButton, sRefuseText) {
            console.log('BEGIN Survey._processSend');

            var that = this,
                oSendData = this._parseBeforeSend();

            oSendData.BUTTON = sButton;
            oSendData.REFUSE_COMMENT = sRefuseText || "";
            this.getView().setBusy(true);
            this._removeCompsErrorState();

            DataAccess.sendSurvey(oSendData).then(
                function () {
                    console.log('BEGIN Survey.onBtnBackPress.sendSurvey.success');
                    if (oSendData.BUTTON != "") {
                        that.getView().setBusy(false);
                        that.onBtnBackPress();
                    } else { // save without status change
                        that._parseSurveyData(false, oSendData);
                        sap.m.MessageToast.show("Сохранено");
                    }
                    console.log('END Survey.onBtnBackPress.sendSurvey.success');
                },
                function (mError) {
                    console.log('BEGIN Survey.onBtnBackPress.sendSurvey.error');
                    that._MessagePopover.getModel().setData(mError.messages);
                    that.getView().getModel("viewData").setProperty("/msgNum", mError.messages.length);
                    that._parseSurveyData(false, oSendData);
                    that._setCompsErrorState(mError.competencies);
                    that.getView().byId("messagePopoverBtn").firePress();
                    console.log('END Survey.onBtnBackPress.sendSurvey.error');
                }
            );
            console.log('END Survey._processSend');
        },

        _removeCompsErrorState: function () {
            console.log('BEGIN Survey._removeCompsErrorState');
            var aCompListItems = this.getView().byId("competenciesList").getItems();
            for (var i = 0; i < aCompListItems.length; i++) {
                aCompListItems[i].removeStyleClass("zCompWithError");
            }
            console.log('END Survey._removeCompsErrorState');
        },

        _setCompsErrorState: function (aCompsWithErrors) {
            console.log('BEGIN Survey.onBtnBackPress.sendSurvey._setCompsErrorState');
            aCompsWithErrors = aCompsWithErrors || [];
            var aCompListItems = this.getView().byId("competenciesList").getItems();

            for (var i = 0; i < aCompListItems.length; i++) {
                var sCompId = aCompListItems[i].data().compId;
                if (aCompsWithErrors.indexOf(sCompId) > -1) {
                    aCompListItems[i].addStyleClass("zCompWithError");
                }
            }
            console.log('END Survey.onBtnBackPress.sendSurvey._setCompsErrorState');
        },

        triggerCompIconFormatter: function (oEvent) {
            console.log('BEGIN Survey.triggerCompIconFormatter');
            var oViewModel = this.getView().getModel("viewData");
            oViewModel.setProperty("/Dummy", oViewModel.getProperty("/Dummy") + 1 || 1);
            console.log('END Survey.triggerCompIconFormatter');
        },

        _parseBeforeSend: function () {
            console.log('BEGIN Survey.onBtnBackPress.sendSurvey._parseBeforeSend');

            var oParsed = $.extend(true, {}, this.oView.getModel("SurveyData").getData());

            $.each(oParsed.Competencies, function (iInd, oComp) {
                    oComp.MARK = oComp.MARK.toString();
                    if (oParsed.IN_AVERAGE) {
                        oComp.CANT_ESTIMATE = oComp.Indicators.every(function (oInd) {
                            return oInd.CANT_ESTIMATE;
                        });
                    }
                    delete oComp._AverageVal;
                    delete oComp._MARK_TO_SHOW;
                    delete oComp._HIDE_INPUT_VALUE;
                $.each(oComp.Indicators, function (iInd, oIndic) {
                    delete oIndic._HIDE_INPUT_VALUE;
                    oIndic.M_VALUE = (+oIndic.M_VALUE).toString();
                    if (oParsed.INDIC_REJECT &&
                        (
                            oParsed.IndicatorScale.SCALE_TYPE == "DragNDrop" ||
                            oParsed.IndicatorScale.SCALE_TYPE == "Dropdown"
                        ) &&
                        oIndic.Q_VALUE == "0000") {
                        oIndic.CANT_ESTIMATE = true;
                    }
                });
            });

            oParsed.CompetencyScale.MIN = oParsed.CompetencyScale.MIN.toString();
            oParsed.CompetencyScale.MAX = oParsed.CompetencyScale.MAX.toString();
            oParsed.CompetencyScale.STEP = oParsed.CompetencyScale.STEP.toString();
            oParsed.IndicatorScale.MIN = oParsed.IndicatorScale.MIN.toString();
            oParsed.IndicatorScale.MAX = oParsed.IndicatorScale.MAX.toString();
            oParsed.IndicatorScale.STEP = oParsed.IndicatorScale.STEP.toString();
            console.log('END Survey.onBtnBackPress.sendSurvey._parseBeforeSend');
            return oParsed;
        },

        onSaveAppraisal: function (oEvt) {
            console.log('BEGIN Survey.onSaveAppraisal');
            this._processSend("");
            console.log('END Survey.onSaveAppraisal');
        },

        onCompleteAppraisal: function (oEvt) {
            console.log('BEGIN Survey.onCompleteAppraisal');
            var that = this;

            MessageBox.confirm("Вы уверены, что хотите завершить оценку?", {
                styleClass: "ZHCM_PM_0650 zCustomMessageBox",
                onClose: function (sAction) {
                    sAction === MessageBox.Action.OK ? that._processSend("ZNEXT") : "";

                }
            });
            console.log('END Survey.onCompleteAppraisal');
        },

        onDeclineToRate: function () {
            console.log('BEGIN Survey.onDeclineToRate');

            var that = this,
                sTextName,
                sFullText = "",
                aDialogContent = [],
                oPerson = this.getView().getModel("Person").getData(),
                oBundle = this.getView().getModel("i18n").getResourceBundle();

            sTextName = oPerson.PERNR === oPerson.APPRAISER ? "SelfRfs" : "Rfs";
            for (var i = 1; i < 11; i++) {
                sFullText += " " + oBundle.getText(sTextName + "." + i);
            }

            aDialogContent.push(new sap.m.FormattedText({htmlText: sFullText}));
            if (oPerson.PERNR !== oPerson.APPRAISER) {
                aDialogContent.push(new sap.m.TextArea("submitDialogTextarea", {
                    width: "100%",
                    placeholder: "Укажите причину отказа"
                }));
            }

            var oCancelBtn = new sap.m.Button({
                text: "Отменить",
                type: "Reject",
                press: function () {
                    dialog.close();
                }
            });

            var dialog = new sap.m.Dialog({
                title: "Подтверждение",
                type: "Message",
                content: aDialogContent,
                initialFocus: oCancelBtn,
                beginButton: new sap.m.Button({
                    text: "Подтвердить",
                    type: "Emphasized",
                    press: function () {
                        var oTextArea = sap.ui.getCore().byId("submitDialogTextarea");
                        var sText = oTextArea ? oTextArea.getValue() : "";

                        dialog.close();
                        that._processSend("ZREFUSE", sText);
                    }
                }),
                endButton: oCancelBtn,
                afterClose: function () {
                    dialog.destroy();
                }
            }).addStyleClass("ZHCM_PM_0650 zCustomMessageBox");

            dialog.open();
            console.log('END Survey.onDeclineToRate');
        },

        handleMessagePopoverPress: function (oEvt) {
            console.log('BEGIN Survey.handleMessagePopoverPress');
            this._MessagePopover.toggle(oEvt.getSource());
            this._MessagePopover.addStyleClass('zMessagePopoverPress').addStyleClass('ZHCM_PM_0650');
            console.log('END Survey.handleMessagePopoverPress');
        },

        onToggleHeader: function () {
            console.log('BEGIN Survey.onToggleHeader');
            var oViewData = this.getView().getModel("viewData");
            oViewData.setProperty("/HeaderExpanded", !oViewData.getProperty("/HeaderExpanded"));
            console.log('END Survey.onToggleHeader');
        },

        onClickTextIndicatorsTableDescription: function (oEvent) {
            console.log('BEGIN Survey.onClickTextIndicatorsTableDescription');

            var oTextControl = oEvent.getSource();
            var sDescrText = oTextControl.getText();
            var oInnerText = oTextControl.getDomRef().firstChild.firstChild;

            if (oTextControl.data) {
                if (oTextControl.data("DESCRIPTION")) {
                    sDescrText = oTextControl.data("DESCRIPTION");
                }
            }

            if (!this._oPopoverIndicatorsTableDescription) {
                this._oPopoverIndicatorsTableDescription = new Popover({
                    placement: sap.m.PlacementType.Right,
                    showHeader: false,
                    enableScrolling: false,
                    content: [new sap.m.Text().addStyleClass("sapUiTinyMargin")]
                });
            }
            this._oPopoverIndicatorsTableDescription
                .getContent()[0]
                .setText(sDescrText);

            this._oPopoverIndicatorsTableDescription.openBy(oInnerText);
            console.log('END Survey.onClickTextIndicatorsTableDescription');
        },

    });


    SimpleType.extend("zhcm_Hap_650.Utils.types.ZSliderValue", {

        constructor : function () {
            console.log('INVOKED types declaration: zhcm_Hap_650.Utils.types.ZSliderValue');
            SimpleType.apply(this, arguments);
            this.sName = "ZSliderValue";
            this.bUseRawValues = true;
        },

        parseValue: function (value) {
            return value;
        },

        formatValue: function (value) {
            return +value;
        },

        validateValue: function () {}
    });


    CompositeType.extend("zhcm_Hap_650.Utils.types.ZSliderInputValue", {

        constructor : function () {
            console.log('INVOKED types declaration: zhcm_Hap_650.Utils.types.ZSliderInputValue');
            CompositeType.apply(this, arguments);
            this.sName = "ZSliderInputValue";
            this.bUseRawValues = true;
        },

        parseValue: function (value) {
            var bNoValue = value === "";
            return [+value, bNoValue]; /*[fM_VALUE, bNO_VALUE]*/
        },

        formatValue: function (aValues/*[fM_VALUE, bNO_VALUE]*/) {
            return aValues[1] ? "" : aValues[0];
        },

        validateValue: function () {}
    });


    CompositeType.extend("zhcm_Hap_650.Utils.types.ZToggleButtonsValue", {

        constructor: function () {
            console.log('INVOKED types declaration: zhcm_Hap_650.Utils.types.ZToggleButtonsValue');
            CompositeType.apply(this, arguments);
            this.sName = "ZToggleButtonsValue";
            this.bUseRawValues = true;
        },

        parseValue: function (value) {
            return [value, +value === 0]; /*[sMARK, bNO_VALUE]*/
        },

        formatValue: function (aValues/*[sMARK, bNO_VALUE]*/) {
            return aValues[0];
        },

        validateValue: function () {}
    });

    return oSurveyController;
});