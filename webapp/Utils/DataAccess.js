sap.ui.define(["zhcm_Hap_650/Utils/Config",
        "sap/ui/model/odata/v2/ODataModel"],
    function (Config, oDataModel) {
        "use strict";

        var DataAccess = {
            getPm360Service: function () {
                console.log('BEGIN: DataAccess.getPm360Service');
                if (!this._oDataModel360) {
                    this._oDataModel360 = new oDataModel(Config.PM_360_SERVICE, {
                        useBatch: false
                    });
                }

                console.log('END: DataAccess.getPm360Service');
                return this._oDataModel360;
            },

            getInstrText: function (sView, sUiElement, sIdEvalProc) {
                console.log('BEGIN: DataAccess.getInstrText');
                var oInstrData = this.getOwnerComponent().getModel("Instructions").getData().d,
                    sText = "";

                if (oInstrData && oInstrData.hasOwnProperty("results")) {
                    $.each(oInstrData.results, function (iInd, oObj) {
                        if (oObj.INTERFACE_ID === sView &&
                            oObj.UI_ELEMENT === sUiElement &&
                            oObj.ID_EVAL_PROC === sIdEvalProc) {

                            sText = oObj.TEXT;
                        }
                    });

                console.log('END: DataAccess.getInstrText');
                    return sText;
                }
            },

            _createPernrFilter: function (sPernrMemId) {
                console.log('BEGIN: DataAccess._createPernrFilter');
                var aFlt = [
                    new sap.ui.model.Filter({
                        path: "PERNR_MEMID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sPernrMemId
                    })
                ];

                console.log('END: DataAccess._createPernrFilter');
                return aFlt;
            },

            executeRead: function (oDataModel, sReadUrl, mUrlParams, aFilters) {
                console.log('BEGIN: DataAccess.executeRead');
                var d = $.Deferred();
                var sMemReadUrl = sReadUrl;

                var s = function (oData, oResponse) {
                    console.log('INVOKED DataAccess.executeRead.success ' + sMemReadUrl );
                    var resultData = oData.results || oData;
                    d.resolve(resultData);
                };
                var e = function (oError) {
                    console.log('INVOKED DataAccess.executeRead.error ' + sMemReadUrl );
                    d.reject();
                };
                var mParameters = {
                    urlParameters: mUrlParams,
                    filters: aFilters,
                    success: s,
                    error: e
                };

                oDataModel.read(sReadUrl, mParameters);
                console.log('END: DataAccess.executeRead');
                return d.promise();
            },

            // Отличается от executeRead тем, что на выход передается сообщение об ошибке
            executeRead2: function (oDataModel, sReadUrl, mUrlParams, aFilters) {
                console.log('BEGIN: DataAccess.executeRead2');

                var d = $.Deferred();

                var s = function (oData, oResponse) {
                    console.log('INVOKED DataAccess.executeRead2.success');

                    var resultData = oData.results || oData;
                    d.resolve(resultData);
                };
                var e = function (oError) {
                    console.log('INVOKED DataAccess.executeRead2.error');
                    d.reject(JSON.parse(oError.responseText));
                };
                var mParameters = {
                    urlParameters: mUrlParams,
                    filters: aFilters,
                    success: s,
                    error: e
                };

                oDataModel.read(sReadUrl, mParameters);
                console.log('END: DataAccess.executeRead2');
                return d.promise();
            },

            getChipData: function () {
                console.log('BEGIN: DataAccess.getChipData');
                var sReadUrl = "/ChipSet";
                console.log('END: DataAccess.getChipData');
                return this.executeRead(this.getPm360Service(), sReadUrl);
            },

            getUserMemId: function () {
                console.log('BEGIN: DataAccess.getUserMemId');
                var oDataModel = new sap.ui.model.odata.v2.ODataModel(Config.ZHCM_KPI_CARDS_SERVICE, {
                    useBatch: false
                });

                var sReadUrl = "/PernrsMemIdSet";
                console.log('END: DataAccess.getUserMemId');
                return this.executeRead(oDataModel, sReadUrl);
            },

            searchForEmpl: function (sSearchString, sAppraisalId, sORGEH) {
                console.log('BEGIN: DataAccess.searchForEmpl');

                var sReadUrl = "/EmplValueHelpSet";
                var aFlt = [];

                if (sAppraisalId) {
                    aFlt.push(new sap.ui.model.Filter({
                        path: "APPRAISALID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sAppraisalId
                    }));
                }
                if (sSearchString) {
                    aFlt.push(new sap.ui.model.Filter({
                        path: "FIO",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sSearchString
                    }));
                }
                if (sORGEH) {
                    aFlt.push(new sap.ui.model.Filter({
                        path: "ORGEH",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sORGEH
                    }));
                }
                console.log('END: DataAccess.searchForEmpl');
                return this.executeRead2(this.getPm360Service(), sReadUrl, {}, aFlt);
            },

            get_sf_orgeh: function (sAppraisalId) {
                console.log('BEGIN: DataAccess.get_sf_orgeh');
                var sReadUrl = "/OrgehValueHelpSet";
                var aFlt = [
                    new sap.ui.model.Filter({
                        path: "APPRAISALID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sAppraisalId
                    })
                ];
                console.log('END: DataAccess.get_sf_orgeh');
                return this.executeRead(this.getPm360Service(), sReadUrl, {}, aFlt);
            },

            getMyTasks: function (sPernrMemId) {
                console.log('BEGIN: DataAccess.getMyTasks');

                var d = $.Deferred(),
                    sReadUrl = "/MyActPanelSet",
                    aFlt = this._createPernrFilter(sPernrMemId);

                var s = function (oData, oResponse) {
                    d.resolve(oData.results);
                };

                var e = function (oError) {
                    d.reject(oError);
                };

                this.getPm360Service().read(sReadUrl, {
                        urlParameters: {"$expand": "ToMyActTable"},
                        filters: aFlt,
                        success: s,
                        error: e
                    }
                );
                console.log('END: DataAccess.getMyTasks');
                return d.promise();
            },

            getSurveyData: function (sAppraisalId, sPernrMemId, fnParse) {
                console.log('BEGIN: DataAccess.getSurveyData');
                var sExpand = "Competencies,Messages,IndicatorScaleValues,CompetencyScaleValues,DescriptionCompScaleValues,DescriptionScaleValues,Competencies/Indicators";
                var that = this;
                this.getPm360Service().metadataLoaded()
                    .then(function () {
                        console.log('DataAccess.getSurveyData.metadataLoaded.success');
                        return that.getPm360Service().createKey("/SurveySet", {
                            PERNR_MEMID: sPernrMemId,
                            APPRAISAL_ID: sAppraisalId
                        });
                    })
                    .then(function (sPath) {
                        console.log('DataAccess.getSurveyData.createKey.success');
                        return that.executeRead(that.getPm360Service(), sPath, {"$expand": sExpand});
                    })
                    .then(function (data) {
                        console.log('DataAccess.getSurveyData.executeRead.success');
                        fnParse(data, true);
                    });
                    console.log('END: DataAccess.getSurveyData');
            },

            getMyMarks: function (sPernrMemId) {
                console.log('BEGIN: DataAccess.getMyMarks');
                var sReadUrl = "/MyMarksPanelSet",
                    sExpand = "ToMyMarksTable,ToCommentScaleM,ToCommentScaleQ,ToCommentScaleM/ToMarkScaleRange";

                var aFlt = this._createPernrFilter(sPernrMemId);

                console.log('END: DataAccess.getMyMarks');
                return this.executeRead(this.getPm360Service(), sReadUrl, {"$expand": sExpand}, aFlt);
            },

            getAddedEmployees: function (sAppraisalId, sPernrMemId) {
                console.log('BEGIN: DataAccess.getAddedEmployees');
                var sExpand = "EmplSetTableSet",
                    that = this;

                return this.getPm360Service().metadataLoaded()
                    .then(function () {
                        console.log('DataAccess.getSurveyData.metadataLoaded.success');
                        return that.getPm360Service().createKey("/EmplSetHeadSet", {
                            PERNR_MEMID: sPernrMemId || "",
                            APPRAISAL_ID: sAppraisalId
                        });
                    })
                    .then(function (sPath) {
                        console.log('DataAccess.getSurveyData.createKey.success');
                        return that.executeRead(that.getPm360Service(), sPath, {"$expand": sExpand});
                    });
                console.log('END: DataAccess.getAddedEmployees');
            },

            getMySubordinates: function (sPernrMemId) {
                console.log('BEGIN: DataAccess.getMySubordinates');
                var sReadUrl = "/ApSubrPanelSet",
                    sExpand = "ToApSubrTable";

                var aFlt = this._createPernrFilter(sPernrMemId);

                console.log('END: DataAccess.getMySubordinates');
                return this.executeRead(this.getPm360Service(), sReadUrl, {"$expand": sExpand}, aFlt);
            },

            getMyReports: function (sPernrMemId) {
                console.log('BEGIN: DataAccess.getMyReports');
                var sReadUrl = "/ReportSet";

                var aFlt = [
                    new sap.ui.model.Filter({
                        path: "PERNR_MEMID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sPernrMemId
                    })
                ];

                console.log('END: DataAccess.getMyReports');
                return this.executeRead(this.getPm360Service(), sReadUrl, {}, aFlt);
            },

            sendSurvey: function (oSurveyData) {
                console.log('BEGIN: DataAccess.sendSurvey');

                var d = $.Deferred();

                var fParseMessages = function (aResultMessages) {
                    var aMessages = [],
                        aCompsWithErrors = [],
                        sMsgty;

                    for (var i = 0; i < aResultMessages.length; i++) {
                        sMsgty = aResultMessages[i].TYPE === "E" ? "Error" : "Warning";
                        aMessages.push({type: sMsgty, title: aResultMessages[i].TEXT});

                        if (aResultMessages[i].COMP_ID) {
                            aCompsWithErrors.push(aResultMessages[i].COMP_ID);
                        }
                    }
                    return {
                        messages: aMessages,
                        competencies: aCompsWithErrors
                    };
                };

                var s = function (oData, oResponse) {
                    console.log('BEGIN: DataAccess.sendSurvey.success');
                    var mParseResult,
                        aMessagesWErrors;
                    if (oData.Messages) {
                        mParseResult = fParseMessages(oData.Messages.results);
                        aMessagesWErrors = $.grep(mParseResult.messages, function (n) {
                            return n.type === "Error";
                        });
                    }
                    if (aMessagesWErrors && aMessagesWErrors.length > 0) {
                        d.reject(mParseResult);
                    } else {
                        d.resolve(oData);
                    }
                    console.log('END: DataAccess.sendSurvey.success');
                };

                var e = function (oError) {
                    console.log('BEGIN: DataAccess.sendSurvey.error');
                    d.reject();
                    console.log('END: DataAccess.sendSurvey.error');
                };

                var mParameters = {
                    urlParameters: {},
                    success: s,
                    error: e
                };

                this.getPm360Service().create("/SurveySet", oSurveyData, mParameters);
                return d.promise();
                console.log('END: DataAccess.sendSurvey');
            },

            sendEmployeeList: function (data) {
                console.log('BEGIN: DataAccess.sendEmployeeList');

                var d = $.Deferred();

                var s = function (oData, oResponse) {
                    d.resolve(oData);
                };

                var e = function (oError) {
                    // oError.error.message.value ?
                    if (oError.responseText) {
                        try {
                            d.reject(JSON.parse(oError.responseText).error.message.value);
                        } catch (e) {
                            d.reject(oError);
                        }
                    } else if (oError.message) {
                        d.reject(oError.message);
                    } else {
                        d.reject(JSON.stringify(oError));
                    }
                };

                var mParameters = {
                    urlParameters: {},
                    success: s,
                    error: e
                };

                this.getPm360Service().create("/EmplSetHeadSet", data, mParameters);
                console.log('END: DataAccess.sendEmployeeList');
                return d.promise();
            },

            getReportFIO: function (afilter, entity) {
                console.log('BEGIN: DataAccess.getReportFIO');

                var sReadUrl = "/ReportFIOSet";
                if (entity === "ReportFIOApre") {
                    sReadUrl = "/ReportFIOApreSet";
                }
                console.log('END: DataAccess.getReportFIO');
                return this.executeRead(this.getPm360Service(), sReadUrl, {}, afilter);
            },

            getReportProc: function (aFilter) {
                console.log('BEGIN: DataAccess.getReportProc');
                var sReadUrl = "/ReportProcSet";
                console.log('END: DataAccess.getReportProc');
                return this.executeRead(this.getPm360Service(), sReadUrl, {}, aFilter);
            },

            getIndividualReport: function (sPernr, sEvalProc360, sFormat) {
                console.log('BEGIN: DataAccess.getIndividualReport');
                sFormat = sFormat || "PDF";
                var sPernrsStringDummy ="";

                var sUrlParams = "REPORTKEY='" + "ZHCM_PM_XXXX_REP_IND" +
                    "',PERNR='" + sPernr +
                    "',ID_EVAL_PROC='" + sEvalProc360 +
                    "',PERNRS_STRING='" + sPernrsStringDummy +
                    "',FORMAT='" + sFormat + "'",

                    sPath = window.location.origin + Config.PM_360_SERVICE
                        + "ReportSet(" + sUrlParams + ")/$value";

                window.open(sPath);
                console.log('END: DataAccess.getIndividualReport');
            },

            getConcReport: function (aPernrs, sEvalProc360, sUserPernr, sFormat) {
                console.log('BEGIN: DataAccess.getConcReport');
                var sPernrsString = "";
                sUserPernr = sUserPernr || "00000000";
                sFormat = sFormat || "PDF";
                for (var i = 0; i < aPernrs.length; i++) {
                    sPernrsString += aPernrs[i] + "_";
                }
                var sUrlParams = "REPORTKEY='" + "ZHCM_PM_XXXX_REP_CONS" +
                    "',PERNR='" + sUserPernr +
                    "',ID_EVAL_PROC='" + sEvalProc360 +
                    "',PERNRS_STRING='" + sPernrsString +
                    "',FORMAT='" + sFormat + "'",

                    sPath = window.location.origin + Config.PM_360_SERVICE
                        + "ReportSet(" + sUrlParams + ")/$value";

                window.open(sPath);
                console.log('END: DataAccess.getConcReport');
            },

            getReportCheck: function(sPernr, sIdEvalProc, sFormatKey, sReportKey) {
                console.log('BEGIN: DataAccess.getReportCheck');

                var sUrlParams = "REPORTKEY='" + sReportKey +
                                "',PERNR='" + sPernr +
                                "',ID_EVAL_PROC='" + sIdEvalProc +
                                "',FORMAT='" + sFormatKey +
                                "'";
                var sReadUrl = "/ReportCheckSet(" + sUrlParams + ")";
                return this.executeRead2(this.getPm360Service(), sReadUrl, {}, {});
                console.log('END: DataAccess.getReportCheck');
            },

            getReportAnonymityCheck : function (sPernr, sIdEvalProc, sAppraisal) {
                console.log('BEGIN: DataAccess.getReportAnonymityCheck');

                var sUrlParams = "PERNR='" + sPernr +
                    "',ID_EVAL_PROC='" + sIdEvalProc +
                    "',APPRAISAL_ID='" + sAppraisal +
                    "'";
                var sReadUrl = "/ReportAnonymCheckSet(" + sUrlParams + ")";
                return this.executeRead2(this.getPm360Service(), sReadUrl, {}, null);
                console.log('END: DataAccess.getReportAnonymityCheck');
            },

        };


        return DataAccess;
    });