sap.ui.define(["./Config"], function (Config) {
    "use strict";

    var Formatter = {

        activeTasks: function (sCount) {
            var rB = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            if (sCount > 0) {
                return "(" + rB.getText("Overview.ActiveTasks") + " " + sCount + ")";
            } else {
                return "";
            }
        },

        procedureStatus: function (sStatus) {
            var rB = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            switch (sStatus) {
                case "Active":
                    return rB.getText("Overview.Procedure.Status.Active");
                case "Completed":
                    return rB.getText("Overview.Procedure.Status.Completed");
            }
        },

        fioLabel: function (reportKey) {
            var labelText = "";
            var i18nModel = new sap.ui.model.resource.ResourceModel({
                bundleUrl: "i18n/i18n.properties"
            });
            var rB = i18nModel.getResourceBundle();
            if (reportKey === "ZHCM_PM_XXXX_REP_IND") {
                labelText = rB.getText("LabelFIOApre");
            } else {
                labelText = rB.getText("LabelFIO");
            }
            return labelText;
        },

        taskStatusIcon: function (sStatus) {
            switch (sStatus) {
                case Config.indicatorId.inProcess || Config.indicatorId.inProcess2:
                    return "sap-icon://pending";
                case Config.indicatorId.Completed:
                    return "sap-icon://complete";
                case Config.indicatorId.notFilled:
                    return "sap-icon://locked";
                case Config.indicatorId.Denied:
                    return "sap-icon://decline";
            }
        },

        taskStatusColor: function (sStatus) {
            switch (sStatus) {
                case Config.indicatorId.inProcess || Config.indicatorId.inProcess2:
                    return "zRedText";
                case Config.indicatorId.Completed:
                    return "zGreenText";
                case Config.indicatorId.notFilled || Config.indicatorId.Denied:
                    return "zGreyText";
                default:
                    break;
            }

            return "";
        },

        procedureStatusTextColor: function (sStatus) {
            switch (sStatus) {
                case "1":
                    return "zBlueText";
                case "0":
                    return "zGreyText";
                default:
                    break;
            }
            return "";
        },

        onlyActiveVisible: function (displayСompleted, sStatus) {
            if (!displayСompleted &&
                (sStatus == Config.indicatorId.Completed ||
                sStatus == Config.indicatorId.Denied ||
                sStatus == Config.indicatorId.notFilled)) {
                return false;
            } else {
                return true;
            }
        },

        onlyActiveGroupVisible: function (displayСompleted, sStatus, innerResults) {
            var notCompletedTasks = [];
            $.each(innerResults, function (key, element) {
                if (element.INDICATOR_ID != Config.indicatorId.Completed) {
                    notCompletedTasks.push(element.INDICATOR_ID);
                }
            });

            if (!displayСompleted && (sStatus === "0" || !notCompletedTasks.length)) {
                return false;
            } else {
                return true;
            }
        },

        competencyValue: function (vMark, vMarkToShow, bCantEstimate, bNoValue, oScale, fAverage, aIndicators, bString) {
            var nMark = +vMark,
                nMarkToShow = +vMarkToShow,                
                bCantEstimateAllIndicators = false,
                bInAverage = oScale.DATA_TYPE === "",
                result, sWhatToShow;

            fAverage = fAverage || 0;

            if (bCantEstimate) {
                return "N";
            } else if (bNoValue && !bInAverage) {
                return "0.0";
            }

            if (bInAverage) {
                sWhatToShow = "AVERAGE";
                bCantEstimateAllIndicators = aIndicators.every(function (oIndic) {
                    return oIndic.CANT_ESTIMATE;
                });
            } else if (oScale.DATA_TYPE === "Q") {
                sWhatToShow = "QVALUE";
            } else if (oScale.DATA_TYPE === "M") {
                sWhatToShow = "MVALUE";
            } else {
                throw new Error("I don't know what to show.");
            }

            switch (sWhatToShow) {
                case ("QVALUE"):
                    if (oScale.SCALE_TYPE === "Table" || oScale.SCALE_TYPE === "Buttons") {
                        result = nMarkToShow || "0.0";
                    } else {
                        result = nMark || "0.0";
                    }
                    break;
                case ("MVALUE"):
                    result = nMark.toFixed(1);
                    if (isNaN(result)) {
                        result = 0;
                    }
                    break;
                case "AVERAGE": // average
                    if (bCantEstimateAllIndicators) {
                        result = "N";
                    } else {
                        result = fAverage.toFixed(1);
                        if (isNaN(result)) {
                            result = 0;
                        }
                    }
                    break;
                default:
                    break;
            }

            return bString ? result.toString() : result;
        },

        sliderEnabled: function (bViewEnabled, bCantEstimate) {
            return (!bViewEnabled || bCantEstimate) ? false : true;
        },

        sliderTextValue: function (mValue, zUnset) {
            return zUnset ? "" : "" + mValue;
        },

        isCompRatingCompleted: function (oCompetence, bInAverage, sIpsScaleNeutralVal, bIndicOblig, sMinIndicToRate, sIndicScaleType) {
            var bIndicatorsAreRated = true,
                iMinIndicToRate = +sMinIndicToRate,
                iCantEstimateIndicNum = 0,
                iEmptyValueIndicNum = 0, // NoValue OR CantEstimate
                iNoValueIndicNum = 0,
                iRatedIndicators = 0,
                iIndicNum = oCompetence.Indicators.length,
                bAllIndicCantEstimate = false,
                bCompHasComments = (oCompetence.COMMENT && oCompetence.COMMENT.length > 0),
                oEventBus = sap.ui.getCore().getEventBus();

            // по среднему: нет, компетенция: нет значения
            if (!bInAverage && oCompetence.NO_VALUE && !oCompetence.CANT_ESTIMATE) {
                return "sap-icon://pending";
            }

            // по среднему: нет, компетенция: не могу оценить
            if (oCompetence.CANT_ESTIMATE) {
                if (bCompHasComments) {
                    oEventBus.publish("isCompRatingCompleted", "iconComplete", oCompetence);
                    return "sap-icon://complete";
                } else {
                    return "sap-icon://pending";
                }
            }

            $.each(oCompetence.Indicators, function (iInd, oIndic) {
                var bEmptyValue;
                var bCantEstimate;
                if (sIndicScaleType === "DragNDrop" || sIndicScaleType === "Dropdown") {
                    bEmptyValue = bCantEstimate = oIndic.Q_VALUE === sIpsScaleNeutralVal;
                } else {
                    bEmptyValue = oIndic.NO_VALUE;
                    bCantEstimate = oIndic.CANT_ESTIMATE;
                }

                if (bEmptyValue) { // NoValue OR CantEstimate
                    iEmptyValueIndicNum++;
                } else {
                    iRatedIndicators++;
                }
                if (bCantEstimate) {
                    iCantEstimateIndicNum++;
                }
                if (bEmptyValue && !bCantEstimate) {
                    iNoValueIndicNum++;
                }
            });

            // Если хоть один показатель с пустым значением - компетенция не оценена
            if (iNoValueIndicNum) {
                return "sap-icon://pending";
            }

            bAllIndicCantEstimate = (iCantEstimateIndicNum === iIndicNum);

            // Оценка показателей обязательна, есть не оцененные либо N
            if (bIndicOblig && iCantEstimateIndicNum) {
                bIndicatorsAreRated = false;
            }

            // Оценка показателей НЕ обязательна, оценено меньше, чем минимальное требование
            if (!bIndicOblig && iMinIndicToRate) {
                bIndicatorsAreRated = iRatedIndicators >= iMinIndicToRate;
            }

            if (bInAverage && bAllIndicCantEstimate && !bCompHasComments) {
                bIndicatorsAreRated = false;
            }

            if (bIndicatorsAreRated) {
                oEventBus.publish("isCompRatingCompleted", "iconComplete", oCompetence);
                return "sap-icon://complete";
            } else {
                return "sap-icon://pending";
            }
        },

        hdrExpandIcon: function (bHdrExpanded) {
            if (bHdrExpanded) {
                return "sap-icon://navigation-right-arrow";
            } else {
                return "sap-icon://navigation-down-arrow";
            }
        },

        photo: function (sPhotoUrl) {
            return sPhotoUrl || "./res/avatar.jpg";
        },

        overviewHeaderWarningVisible: function (iActiveTasks, aPernrsAmList, sCurrentPernr) {
            var bResult = false,
                sPernr;
            if (iActiveTasks !== undefined && aPernrsAmList && sCurrentPernr) {
                bResult = !!aPernrsAmList.reduce(function (acc, oPernr) {
                    sPernr = Object.keys(oPernr)[0];
                    if (sPernr !== sCurrentPernr) {
                        acc += oPernr[sPernr];
                    }
                    return acc;
                }, 0);
            }
            return bResult;
        },

        indicatorCropText: function(sText){
            var maxLength = 50;
            var textLength = sText.length;

            if (textLength > maxLength) {
                sText = sText.substring(0, maxLength) + "...";
            }

            return sText;
        }
    };

    return Formatter;

}, true);