sap.ui.define([
    "sap/ui/core/routing/History",
    "sap/ui/core/UIComponent",
    "sap/ui/core/routing/HashChanger"
],
function (History, UIComponent, HashChanger) {
    'use strict';

    var Utilits = {
        navBack: function(oView, isMobile) {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();
            var oRouter = UIComponent.getRouterFor(oView);
            var oHashChanger = null;
            var sCurrentHash = "";
            
            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                oHashChanger = new HashChanger();

                if(oHashChanger) {
                    sCurrentHash = oHashChanger.getHash() || "";

                    //check it's not empty and nav to Main
                    if(sCurrentHash.length > 0)
                    {
                        if(isMobile) oRouter.navTo("mOverview", true);
                        else oRouter.navTo("overview", true);
                    }
                }
            }
        }
    };

    return Utilits;
})