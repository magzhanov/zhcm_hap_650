module.exports = function (grunt) {

    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        clean: [
            'zhcm_Hap_650.zip',
            'Component-preload.js'
        ],

        gitInfo: {},

        openui5_preload: {
            zhcm_Hap_650: {
                options: {
                    resources: {
                        cwd: "./",
                        prefix: "zhcm_Hap_650",
                        src: [
                            "**/*.js",
                            "**/*.xml",
                            "**/*.properties",
                            "!Gruntfile.js",
                            "!package.json",
                            "!ui5pkg.json",
                            "!node_modules/**",
                            "!**/web.xml",
                            "!**/*-preload.json",
                            "!**/*-preload.js",
                            "!**/*-dbg.js"
                        ]
                    },
                    compress: {
                        uglifyjs: {
                            compress: {
                                screw_ie8: false
                            },
                            mangle: {
                                screw_ie8: false
                            }
                        }
                    },
                    dest: "./"
                },
                components: "zhcm_Hap_650"
            }
        },

        compress: {
            main: {
                options: {
                    archive: 'zhcm_Hap_650.zip'
                },
                files: [{
                    expand: true,
                    src: [
                        '**/**',
                        '!node_modules/**',
                        '!*.zip',
                        '!Gruntfile.js',
                        '!ui5pkg.json',
                        '!package.json'
                    ],
                    dest: '/'
                }]
            }
        }

    });

    grunt.registerTask('default', ['clean', 'gitInfo', 'openui5_preload', 'compress']);
};
