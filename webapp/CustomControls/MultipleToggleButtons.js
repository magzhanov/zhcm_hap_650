sap.ui.define([
    "sap/ui/core/Control"
], function (Control) {
    "use strict";

    var MultipleToggleButtons = Control.extend("zhcm_Hap_650.CustomControls.MultipleToggleButtons", {
        metadata: {
            properties: {
                scale:       {type: "object"},
                displayText: {type: "boolean", defaultValue: true},
                enabled:     {type: "boolean", defaultValue: true},
                cantEstimateAvailable: {type: "boolean", defaultValue: true},
                cantEstimateVal:       {type: "boolean"},
                value:       {type: "string"},
                valueToShow: {type: "string"},
                spacing:     {type: "int", defaultValue: 32} //px

            },
            aggregations: {
                _buttons: {type: "sap.m.ToggleButton", multiple: true},
                _nButton: {type: "sap.m.ToggleButton", multiple: false}
            },
            events: {
                /**
                 * This event is triggered if average mark has to be calculated.
                 */
                calcAverage: {
                    parameters: {
                        rootControl: {type: "Object"}
                    }
                }
            }
        },

        _onButtonPress: function (oEvt) {
            var oSource = oEvt.getSource(),
                oTopControl = oSource.getParent(),
                aButtons = oTopControl.getAggregation("_buttons"),
                oNButton = oTopControl.getAggregation("_nButton"); 

            if (oNButton) {
                aButtons.push(oNButton);
            }
            $.each(aButtons, function (sIndex, oButton) {
                if (oButton.getId() !== oSource.getId()) {
                    oButton.setPressed(false);
                } else {
                    if (oButton.getPressed()) {
                        oTopControl.setCantEstimateVal(oButton.data("valToSet") === "0000");
                        oTopControl.setValue(oButton.data("valToSet"));
                        oTopControl.setValueToShow(oButton.data("valToShow"));
                    } else {
                        oTopControl.setCantEstimateVal(false);
                        oTopControl.setValue("0000");
                    }
                }
            });

            this.fireCalcAverage({ rootControl: oEvt.getSource().getParent() });

        },

        onBeforeRendering: function () {
            if (this.getAggregation("_buttons")) {
                return;
            }

            if (this.getCantEstimateAvailable()) {
                var oNButton = new sap.m.ToggleButton({
                    text: "N",
                    pressed: this.getCantEstimateVal(),
                    press: this._onButtonPress.bind(this)
                });
                oNButton.addStyleClass("zRatingToggleBtn");
                oNButton.addStyleClass("zRatingToggleCantBtn");
                oNButton.data("valToSet", "0000");
                oNButton.data("valToShow", "N");
                this.setAggregation("_nButton", oNButton);
            }

            var oScaleVals = this.getScale(),
                that = this,
                nCurrentMark = this.getValue();

             $.each(oScaleVals, function (sIndex, oScale) {
                var oButton = new sap.m.ToggleButton({
                    text: +oScale.VALUE,
                    pressed: +nCurrentMark === +oScale.ID,
                    press: that._onButtonPress.bind(that),
                    enabled: that.getEnabled()
                });
                oButton.addStyleClass("zRatingToggleBtn");
                oButton.data("valToSet", +oScale.ID);
                oButton.data("valToShow", +oScale.VALUE);
                that.addAggregation("_buttons", oButton);
            });
        },

        renderer: {

            render: function (oRm, oControl) {
                var bDisplayText = oControl.getDisplayText(),
                    aButtons = oControl.getAggregation("_buttons") || [],
                    bCantEstimateAvailable = oControl.getCantEstimateAvailable(),
                    oScaleVals = oControl.getScale(),
                    iSpacing = oControl.getSpacing();

                oRm.write("<div");
                oRm.writeControlData(oControl);
                oRm.addClass("zhcm_Hap_650MultipleToggleButtons");
                oRm.writeClasses();
                oRm.write(">");

                oRm.write("<div class='zhcm_Hap_650MarkBoxContainer'>");

                try {
                    var iColumns = this._getColumnCount(aButtons.length),
                        iRows = Math.ceil(aButtons.length / iColumns),
                        sWidth = this._getColumnWidth(bCantEstimateAvailable ? iColumns + 1 : iColumns, iSpacing);

                    for (var r = 0; r < iRows; r++) {
                        for (var c = 0; c < iColumns; c++) {
                            var i = r * iColumns + c,
                                oButton = aButtons[i];
                            if (oButton) {
                                this._renderButton(oRm, oButton, sWidth, "Normal", bDisplayText ? oScaleVals[i].DESCRIPTION : null);
                            } else {
                                this._renderButton(oRm, null, sWidth, "Empty");
                            }
                        }
                        if (bCantEstimateAvailable) {
                            if (r === 0) {
                                this._renderButton(oRm, oControl.getAggregation("_nButton"), sWidth, "NButton",
                                    bDisplayText ? 'Не могу оценить' : null);
                            } else {
                                this._renderButton(oRm, null, sWidth, "Empty");
                            }
                        }
                    }
                } catch (e) {}

                oRm.write("</div>");
                oRm.write("</div>");
            },

            _renderButton: function (oRm, oButton, sWidth, sType, sText) {
                oRm.write("<div");
                oRm.addClass("zhcm_Hap_650MarkBox");
                if (sType === "NButton") {
                    oRm.addClass("zhcm_Hap_650MarkBoxCE");
                }
                oRm.addStyle("width", sWidth);
                oRm.writeStyles();
                oRm.writeClasses();
                oRm.write(">");

                if (sType !== "Empty") {
                    oRm.renderControl(oButton);
                    if (sText) {
                        oRm.write("<div class='zhcm_Hap_650ScaleValText'>" + sText + "</div>");
                    }
                }

                oRm.write("</div>");
            },

            _getColumnCount: function (iButtonsCount) {
                switch (iButtonsCount) {
                    case 1:
                    case 2:
                        return iButtonsCount;
                    case 3:
                    case 6:
                    case 9:
                        return 3;
                    case 4:
                    case 7:
                    case 8:
                    case 11:
                    case 12:
                        return 4;
                    default:
                        return 5;
                }
            },

            _getColumnWidth: function (iColumns, iSpacing) {
                var sWidth = (100 / iColumns).toFixed(2) + "%";
                return "calc(" + sWidth + " - 0.2em)"; 
            },

        },


    });

    return MultipleToggleButtons;

});
