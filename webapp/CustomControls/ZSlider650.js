sap.ui.define([
    "jquery.sap.global",
    "sap/ui/core/Control",
    "sap/m/Slider",
    "sap/m/Input",
    "sap/ui/core/EnabledPropagator",
    "sap/m/ResponsivePopover"
], function ($, Control, Slider, Input, EnabledPropagator, ResponsivePopover) {
    "use strict";

    var ZSlider650 = Control.extend("zhcm_Hap_650.CustomControls.ZSlider650", {

        metadata: {
            properties: {
                scale:   {type: "any"}, // expects Object
                width:   {type: "sap.ui.core.CSSSize", group: "Appearance", defaultValue: "100%"},
                name:    {type: "string", group: "Misc", defaultValue: "ZSlider650"},
                min:     {type: "float", defaultValue: 0},
                max:     {type: "float", defaultValue: 100},
                step:    {type: "float", defaultValue: 1},
                value:   {type: "any", defaultValue: 0},
                inputValue:     {type: "any", defaultValue: 0},
                noValue:        {type: "boolean", defaultValue: true},
                enabled: {type: "boolean", defaultValue: true},
                mobile:  {type: "boolean", defaultValue: false}
            },
            aggregations: {
                _slider: {type: "sap.m.Slider", multiple: false},
                _input:  {type: "sap.m.Input", multiple: false}
            },
            events: {
                /**
                 * This event is triggered if average competency mark has to be calculated.
                 */
                calcAverage: {
                    parameters: { emittingControl: {type: "Object"} }
                },
                inputChange: {
                    parameters: {
                        emittingControl: {type: "Object"},
                        newValue: {type: "any"}
                    }
                }
            }
        },

        CONST: {
          trackNamePart: "-zslider650-track"
        },

        // =======================================================================================================================================================================
        // INIT & LIFECYCLE
        // =======================================================================================================================================================================

        constructor: function (mSettings) {
            var that = this,
                oSlider;

            Control.apply(this, arguments);
            this._bSetValueFirstCall = true;

            if (mSettings.scale) {
                oSlider = new Slider({
                    min: mSettings.min,
                    max: mSettings.max,
                    step: mSettings.step,
                    width: mSettings.width,
                    value: mSettings.value,
                    enableTickmarks: true,
                    progress: false,
                    showAdvancedTooltip: false,
                    showHandleTooltip: false,
                    enabled: mSettings.enabled,
                    name: mSettings.name || this.createGuid()
                });

                oSlider.setValue = function(vNewValue) {
                    var sValue = (vNewValue === 0 || vNewValue === "") ? 0 : vNewValue;
                    return Slider.prototype.setProperty.call(this, "value", sValue, true /*bSuppressInvalidate*/);
                 };

                this.setAggregation("_slider", oSlider);

                var oInput = new Input({
                    value: mSettings.inputValue,
                    type: sap.m.InputType.Number,
                    enabled: mSettings.enabled
                }).addStyleClass("zSliderInput");

                this.setAggregation("_input", oInput);

                if(mSettings.mobile) {
                    oInput.addStyleClass("zMobSliderInput");
                }

                // waiting for the DOM render
                $.sap.delayedCall(0, null, function () {
                     oInput.$().find("input").on("focus", function (oEvt) {
                         $(oEvt.target).select(); // select all text on focus
                     });
                });
            }

            oSlider = this.getAggregation("_slider");
            oSlider.attachEvent("liveChange", this._onRate.bind(this));
            oSlider.attachEvent("change", this._onChange.bind(this));
            this.getAggregation("_input").attachEvent("change", this._onInputChange.bind(this));

            oSlider.onfocusout = function() {
                // Магия. Не трогать. Фиксит следующий баг: в показателях кликаем на первый слайдер, на второй слайдер, на третий слайдер,
                // потом на кружок последнего слайдера и тянем его. Двигаться будут либо последние 2 либо все 3 слайдера.
                $.sap.delayedCall(0, null, function () { $("#homeBtn").focus(); });

                Slider.prototype.onfocusout.apply(this, arguments);
                that._onChange({getSource: function () {return oSlider;}});
            };
        },

        onAfterRendering: function () {
            this._changeDeafultStyles();
            this._addColorIfNotSet(this.getScale());

            if (this.getMobile()) {
                this._applyMobileOverrides();
            } else {
                this._applyDesktopOverrides();
            }

            this._onRate(null, true);
        },

        onExit: function () {
            if (this._oPopover) {
                this._oPopover.destroy();
            }
        },

        _applyDesktopOverrides: function() {
            var that = this;

            $.each(this.getScale(), function (sIndex, oScale) {
                var nTrackPartWidth = (((oScale.MAX - oScale.MIN) / (that.getMax() - that.getMin())) * 100).toFixed(1);
                var text = oScale.DESCRIPTION.substring(0, 32) + " ...<br/>(" + oScale.MIN + " - " + oScale.MAX + ")";

                var elem = $("<div>", {
                    id: that.getName() + "-zslider650-part" + sIndex,
                    class: "zslider650-part",
                    style: "background-color: " + oScale.COLOR + "; width: " + nTrackPartWidth + "%;",
                    html: $("<span>", {
                        id: that.getName() + "-zslider650-part-text-" + sIndex,
                        class: "zslider650-part-text",
                        html: text
                    })
                });

                that.$().find(".zslider650-track").append(elem);
            });

            this.$().find(".zslider650-part-text")
                .on("mousedown", this._onClickPartText.bind(this));
        },

        _applyMobileOverrides: function() {
            var that = this;
            $.each(this.getScale(), function (sIndex, oScale) {
                var nTrackPartWidth = (((oScale.MAX - oScale.MIN) / (that.getMax() - that.getMin())) * 100).toFixed(1);
                var elem = $("<div>", {
                    id: that.getName() + "-zslider650-part" + sIndex,
                    class: "zslider650-part",
                    style: "background-color: " + oScale.COLOR + "; width: " + nTrackPartWidth + "%;",
                    html: $("<span>", {
                        id: that.getName() + "-zslider650-part-text-" + sIndex,
                        class: "zslider650-part-text"
                    })
                });

                that.$().find(".zslider650-track").append(elem);
            });
        },

        _changeDeafultStyles: function () {
            var sTrackId = this.getName() + this.CONST.trackNamePart;

            this.$().find(".sapMSliderInner")
                .css("background-color", "transparent");
            this.$().find(".sapMSliderHandle")
                .css("background-color", "white")
                .css("box-shadow", "2px 2px 8px rgba(0,0,0,0.5)");
            this.$().find(".sapMSliderLabel")
                .css("color", "transparent");

            this.$().find(".sapMSliderTickmarks")
                .before('<div id="' + sTrackId + '" class="zslider650-track"></div>');

            var bIsPhone = function(){
                var res = document.clientWidth > 500 ? false:true;
                return res;
            };
            if (bIsPhone()) {
                var aParentBox = $('.zIndRoundedFlexBox');

                $('.ZHCM_PM_0650 .zMobSliderInput').css('top', '-30px');

                for (var i = 0; i < aParentBox.length; i++) {
                    if (aParentBox[i].children[1].children[0].children.length > 1) {
                        if (aParentBox[i].children[1].children[0].children[1].tagName == "DIV") {
                            $("#"+aParentBox[i].children[1].children[0].children[1].id).css('top',
                            'calc(-'+ aParentBox[i].children[0].scrollHeight +'px)');
                        }
                    }
                }
            }
        },

        _addColorIfNotSet: function(aScaleParts) {
            $.each(aScaleParts, function (sIndex, oScale) {
                if (!this._isColor(oScale.COLOR)) {
                    oScale.COLOR = "lightgray";
                }
            }.bind(this));
        },

        renderer: {
            render: function (oRM, oControl) {
                var sMarginClass = oControl.getMobile() ? "sapUiSmallMarginTop" : "sapUiMediumMarginBottom";

                oRM.write("<div");
                oRM.writeControlData(oControl);
                oRM.addClass("zslider650");
                oRM.addClass(sMarginClass);
                oRM.writeClasses();
                oRM.write(">");
                oRM.renderControl(oControl.getAggregation("_slider"));
                oRM.renderControl(oControl.getAggregation("_input"));
                oRM.write("</div>");
            }
        },

        // =======================================================================================================================================================================
        // GETTERS & SETTERS
        // =======================================================================================================================================================================
        setName: function() {
            var sCtrlName = this.getName() || this.createGuid(),
                oSlider = this.getAggregation("_slider");
            if (oSlider) {
                oSlider.setProperty("name", sCtrlName, true);
            }
        },

        setValue: function (value) {
            return Slider.prototype.setProperty.call(this, "value", value, true /*bSuppressInvalidate*/);
        },
        setInputValue: function (value) {
            return Slider.prototype.setProperty.call(this, "inputValue", value, true /*bSuppressInvalidate*/);
        },
        setNoValue: function (value) {
            return Slider.prototype.setProperty.call(this, "noValue", value, true /*bSuppressInvalidate*/);
        },

        // =======================================================================================================================================================================
        // HANDLERS
        // =======================================================================================================================================================================

        _onChange: function (oEvt) {
            this._bSetValueFirstCall = true;
            this._onRate(oEvt);
            this.fireCalcAverage({
                emittingControl: oEvt.getSource(),
                rootControl: oEvt.getSource().getParent()
            });
        },

        _onInputChange: function (oEvt) {
            var sNewValue = oEvt.getParameter("newValue");
            var fNewValue = +sNewValue;
            this.getAggregation("_slider").setValue(fNewValue);
            this._onChange(oEvt);
            this.fireInputChange({
                emittingControl: oEvt.getSource(),
                rootControl: oEvt.getSource().getParent(),
                newValue: fNewValue
            });
        },

        _onRate: function (oEvt, bIgnoreNoValue) {
            var nIndex = null;
            var value = this.getAggregation("_slider").getValue();
            var parts = this.$().find(".zslider650-track > div");
            var part = null;
            var oScaleVals = this.getScale() || [];
            var oViewModel = this.getModel("viewData");
            var bSkipSetValue = false;
            if (oViewModel) {
                bSkipSetValue = oViewModel.getProperty("/JustPressedCantEstimate") === this.getBindingContext("SurveyData");
            }

            if (this._bSetValueFirstCall) {
                this._bSetValueFirstCall = false;
            } else {
                var oInput = this.getAggregation("_input");
                if (!bIgnoreNoValue) {
                    this.setNoValue(false);
                    oInput.setValue(+value);
                } else {
                    if (this.getNoValue()) {
                        oInput.setValue("");
                    } else {
                        if (bSkipSetValue) {
                            this.getModel("viewData").setProperty("/JustPressedCantEstimate", false);
                            oInput.setValue("");
                        } else {
                            oInput.setValue(+value);
                        }
                    }
                }
            }

            $.each(oScaleVals, function (sIndex, oScale) {
                if ((part === null) && (value <= oScale.MAX)) {
                    nIndex = sIndex;
                    part = parts[sIndex];
                }
            });

            var prevPart = this.$().find(".zslider650-track .zslider650-active-part");
            if (prevPart) {
                $(prevPart).removeClass("zslider650-active-part");
            }

            if (nIndex !== null) {
                var bkcolor = oScaleVals[nIndex].COLOR;
                this.$().find(".sapMSliderHandleTooltip")
                    .css("color", bkcolor)
                    .css("font-weight", "bold");

                this.$().find(".zSliderInput input")
                    .css("color", bkcolor);
            }
        },

        _onClickPartText: function (event) {
            event.stopImmediatePropagation();
            var actionCtrl = event.target;  // event.eventPhase = 2 !!!
            var arr = actionCtrl.id.split("-");
            var sIndex = arr[arr.length - 1];

            var part = this.getScale()[sIndex];
            var str = part.DESCRIPTION + ", (" + part.MIN + " - " + part.MAX + ")";

            if (!this._oPopover) {
                var oText = new sap.m.Text();
                oText.addStyleClass("zslider650-active-part-tooltip");

                this._oPopover = new ResponsivePopover({
                    title: "Уровень шкалы",
                    contentWidth: "250px",
                    placement: "Bottom",
                    content: [oText]
                });
                this._oPopover.addStyleClass("sapUiPopupWithPadding");
            }
            this._oPopover.getContent()[0].setText(str);
            this._oPopover.openBy(actionCtrl);
        },

        // =======================================================================================================================================================================
        // UTILITY
        // =======================================================================================================================================================================

        _isColor: function (sColor) {
            if (!sColor || sColor.length !== 7) {
                return false;
            }

            var s = new Option().style;
            s.color = sColor;

            var test1 = s.color === sColor;
            var test2 = /^#[0-9A-F]{6}$/i.test(sColor);

            return test1 || test2 ? true : false;
        },

        createGuid: function () {
            return "xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0,
                    v = c === "x" ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    });

    EnabledPropagator.call(ZSlider650.prototype);

    return ZSlider650;
});