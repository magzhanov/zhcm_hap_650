sap.ui.define(['sap/m/SelectList',
        'sap/ui/core/Element',
        'sap/ui/Device',
        'sap/ui/core/Icon'],
    function(SelectList, Element, Device, Icon) {
        "use strict";

        var zSelectList = SelectList.extend("zhcm_Hap_650.CustomControls.zSelectList", {
            renderer: {
                renderItem : function(oRm, oList, oItem, mStates) {

                    if (!(oItem instanceof Element)) {
                        return;
                    }

                    var bEnabled = oItem.getEnabled(),
                        sColor = oItem.getColor(),
                        oSelectedItem = oList.getSelectedItem(),
                        CSS_CLASS = sap.m.SelectListRenderer.CSS_CLASS,
                        sTooltip = oItem.getTooltip_AsString(),
                        bShowSecondaryValues = oList.getShowSecondaryValues();

                    oRm.write("<li");

                    if (mStates.elementData) {
                        oRm.writeElementData(oItem);
                    }

                    if (oItem instanceof sap.ui.core.SeparatorItem) {
                        oRm.addClass(CSS_CLASS + "SeparatorItem");

                        if (bShowSecondaryValues) {
                            oRm.addClass(CSS_CLASS + "Row");
                        }
                    } else {

                        oRm.addClass(CSS_CLASS + "ItemBase");

                        if (bShowSecondaryValues) {
                            oRm.addClass(CSS_CLASS + "Row");
                        } else {
                            oRm.addClass(CSS_CLASS + "Item");
                        }

                        if (oItem.bVisible === false) {
                            oRm.addClass(CSS_CLASS + "ItemBaseInvisible");
                        }

                        if (!bEnabled) {
                            oRm.addClass(CSS_CLASS + "ItemBaseDisabled");
                        }

                        if (bEnabled && Device.system.desktop) {
                            oRm.addClass(CSS_CLASS + "ItemBaseHoverable");
                        }

                        if (oItem === oSelectedItem) {
                            oRm.addClass(CSS_CLASS + "ItemBaseSelected");
                        }

                        if (bEnabled) {
                            oRm.writeAttribute("tabindex", "0");
                        }

                        if(sColor){
                            oRm.addStyle('color', sColor);
                            oRm.writeStyles();
                        }
                    }

                    oRm.writeClasses();

                    if (sTooltip) {
                        oRm.writeAttributeEscaped("title", sTooltip);
                    }

                    this.writeItemAccessibilityState.apply(this, arguments);

                    oRm.write(">");

                    if (bShowSecondaryValues) {

                        oRm.write("<span");
                        oRm.addClass(CSS_CLASS + "Cell");
                        oRm.addClass(CSS_CLASS + "FirstCell");
                        oRm.writeClasses();
                        oRm.writeAttribute("disabled", "disabled"); // fixes span obtaining focus in IE
                        oRm.write(">");

                        this._renderIcon(oRm, oItem);

                        oRm.writeEscaped(oItem.getText());
                        oRm.write("</span>");

                        oRm.write("<span");
                        oRm.addClass(CSS_CLASS + "Cell");
                        oRm.addClass(CSS_CLASS + "LastCell");
                        oRm.writeClasses();
                        oRm.writeAttribute("disabled", "disabled"); // fixes span obtaining focus in IE
                        oRm.write(">");

                        if (typeof oItem.getAdditionalText === "function") {
                            oRm.writeEscaped(oItem.getAdditionalText());
                        }

                        oRm.write("</span>");
                    } else {
                        this._renderIcon(oRm, oItem);

                        oRm.writeEscaped(oItem.getText());
                    }

                    oRm.write("</li>");
                },

                _renderIcon : function(oRm, oItem) {
                    if (oItem.getIcon && oItem.getIcon()) {
                        var oIcon = new Icon({src: oItem.getIcon()});
                        oIcon.addStyleClass("sapMSelectListItemIcon");
                        oRm.renderControl(oIcon);
                    }
                }
            }
        });

        return zSelectList;

    });
