sap.ui.define(['sap/m/Select'],
    function(Select) {
        "use strict";

        var zSelectIncaps = Select.extend("zhcm_Hap_650.CustomControls.zSelectIncaps", {
            metadata : {
                properties : {
                    color : {type : "string", defaultValue : null}
                }
            },

            renderer: {},

            onBeforeOpen : function() {
                Select.prototype.onBeforeOpen.apply(this, arguments);

                setTimeout(function(){
                    var dialogs = document.getElementsByClassName('sapMDialog');
                    for (var i = 0; i < dialogs.length; i++) {
                        dialogs[i].classList.add("ZHCM_PM_0650")
                    }
                }, 10);
            }
        });


        return zSelectIncaps;
    }
);