sap.ui.define(['sap/ui/core/ListItem'],
    function(ListItem) {
        "use strict";

        var zColoredListItem = ListItem.extend("zhcm_Hap_650.CustomControls.zColoredListItem", {
            metadata : {
                properties : {
                    color : {type : "string", group : "Appearance", defaultValue : null},
                }
            }});

        return zColoredListItem;

    });
