sap.ui.define(['sap/m/ComboBox'],
    function(ComboBox) {
        "use strict";

        var zComboBoxIncaps = ComboBox.extend("zhcm_Hap_650.CustomControls.zComboBoxIncaps", {
            metadata : {
                properties : {
                    color : {type : "string", defaultValue : null}
                }
            },

            renderer: {},

            onBeforeOpen : function() {
                ComboBox.prototype.onBeforeOpen.apply(this, arguments);

                setTimeout(function(){
                    var dialogs = document.getElementsByClassName('sapMDialog');
                    for (var i = 0; i < dialogs.length; i++) {
                        dialogs[i].classList.add("ZHCM_PM_0650")
                    }
                }, 10);
            }
        });


        return zComboBoxIncaps;
    }
);