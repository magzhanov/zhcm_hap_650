// some text
sap.ui.define([
    "sap/m/Text",
    "sap/m/TextRenderer"
], function (Text, TextRenderer) {
    "use strict";

    var oZClickableText = Text.extend("zhcm_Hap_650.CustomControls.ZClickableText", {
        metadata: { events: { press: {} } },
        ontap: function () { this.firePress({}); }
    });
    zhcm_Hap_650.CustomControls.ZClickableTextRenderer = TextRenderer; 
    
    return oZClickableText;
});